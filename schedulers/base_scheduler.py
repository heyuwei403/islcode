from torch.optim.lr_scheduler import _LRScheduler

class BaseScheduler(_LRScheduler):

    @staticmethod
    def modify_commandline_options(parser):
        return parser

    def __init__(self, optimizer, opt):
        # super(BaseScheduler, self).__init__(optimizer)
        self.optimizer = optimizer
        self.wait_epoch = 0
        self.last_epoch = 0
        self.lr = opt.lr
        self.opt = opt
        self.warm_epoch = opt.warm_epoch
        self.warm_start_lr = self.lr * opt.warm_start_factor


        if self.opt.with_warm_up:
            self.lr = self.warm_start_lr
        else:
            self.lr = self.opt.lr

        self.assign_lr()


    def step(self, epoch=None):

        if epoch is None:
            epoch = self.last_epoch + 1
        self.last_epoch = epoch

        if self.opt.with_warm_up and epoch < self.opt.warm_epoch:
            self.lr = self.warm_up(epoch)


        self.assign_lr()


    def warm_up(self, epoch):
        if epoch >= self.warm_epoch:
            return self.lr

        warm_epoch = self.opt.warm_epoch
        lr_bias = self.opt.lr - self.warm_start_lr
        lr = self.warm_start_lr + lr_bias * epoch / (warm_epoch - 1)
        return lr

    def assign_lr(self):
        for param_group in self.optimizer.param_groups:
            if 'lr_mult' in param_group.keys():
                param_group['lr'] = self.lr * param_group['lr_mult']
            else:
                param_group['lr'] = self.lr
