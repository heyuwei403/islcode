import os
from options.my_options import TestOptions
from datasets import create_dataset
from models import create_model
from util.visualizer import Visualizer
import time
import os
import torch
# torch.backends.cudnn.enabled = False
os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'

if __name__ == '__main__':
    opt = TestOptions().parse()  # get test options
    opt.serial_batches = True

    if opt.l_state == 'train':
        opt.l_state = 'valid'

    # opt.l_state = 'valid'

    visualizer = Visualizer(opt)
    # v_dataset = create_dataset(opt, 'train')
    # # v_dataset = create_dataset(opt, 'valid')
    #
    if opt.test_train_data:
        v_dataset = create_dataset(opt, 'train')
    else:
        v_dataset = create_dataset(opt, 'valid')
    print('The number of test images = %d' % len(v_dataset))

    model = create_model(opt)
    model.setup(opt)

    if hasattr(model, 'set_center'):
        model.set_center(opt)

    v_start_time = time.time()
    model.validation(v_dataset, visualizer, valid_iter=-1)
    model.change_dir_name(visualizer)
