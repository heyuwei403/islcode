import numpy as np
from PIL import Image
import torch
import cv2
import os
from .basic import mkdir
import copy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def norm2image(image,minp=None,maxp=None):
    if minp is None:
        if isinstance(image, np.ndarray):
            minp = np.min(image)

        if isinstance(image, torch.Tensor):
            minp = torch.min(image)

    if maxp is None:
        if isinstance(image, np.ndarray):
            maxp = np.max(image)

        if isinstance(image, torch.Tensor):
            maxp = torch.max(image)

    image = image - minp
    maxp = maxp - minp

    image = image * (255 / maxp)

    if isinstance(image, np.ndarray):
        image = image.astype('uint8')

    if isinstance(image, torch.Tensor):
        image = image.short()

    return image

def tensor2im(images, intype, outtype = 'NHWC', minp = None, maxp = None, norm_to_255 = True):
    """only process one image"""


    """"Converts a Tensor array into a numpy image array with N x H x W x C.
        When mode is 'gray', C = 1

    Parameters:
        input_image (tensor) --  the input image tensor array
        imtype (type)        --  the desired type of the converted numpy array
    """


    if isinstance(images, torch.Tensor):  # get the data from a variable
        images = images.cpu().float().numpy()  # convert it into a numpy array

    image_shape = images.shape
    assert len(intype) == len(image_shape)

    intype = intype.upper()

    if 'N' not in intype:
        images = images[np.newaxis,:]
        intype = 'N' + intype

    if 'C' not in intype:
        images = images[np.newaxis,:]
        intype = 'C' + intype

    trans_tuple = [0] * 4
    for i, e in enumerate(intype):
        trans_tuple[outtype.index(e)] = i

    trans_tuple = tuple(trans_tuple)
    images = np.transpose(images, trans_tuple)

    C_index = outtype.lower().index('c')
    if images.shape[C_index] == 1:
        images = np.repeat(images, 3, axis = C_index)

    if norm_to_255:
        if isinstance(minp,type(None)):
            minp = np.min(images)

        if isinstance(maxp, type(None)):
            maxp = np.max(images)

        images = images - minp
        maxp = maxp - minp

        images = images * (255 / maxp)
        images = images.astype('uint8')
    return images

def cat_image(images,h_num = 10,factor = 0.75,in_type='HWC', out_type = 'HWC'):
    """

    Args:
        images: iamges with the same shape N x C x H x W or N x H x W x C
        h_num: images per line, -1 means all images in one line
        factor: the ratio factor for image size
        in_type: HWC or CHW
        out_type: HWC or CHW

    Returns:

    """
    if isinstance(images, torch.Tensor):
        images = images.cpu().float().numpy()  # convert it into a numpy array

    if isinstance(images, list):
        for i, im in enumerate(images):
            if isinstance(im, torch.Tensor):
                images[i] = im.cpu().float().numpy()

        images = np.stack(images, axis= 0)

    if in_type == 'HWC':
        images = np.transpose(images, (0, 3, 1, 2))

    if h_num == -1:
        h_num = images.shape[0]

    line_num = images.shape[0] // h_num
    rest_num = images.shape[0] - line_num * h_num

    image_data = []

    width = images.shape[-1]
    height = images.shape[-2]
    for i in range(line_num):
        image_line = np.zeros((images.shape[1],height,width*h_num))
        for j in range(h_num):
            image_line[:,:,j*width:(j+1)*width] = images[h_num*i + j]
        image_data.append(image_line)

    if rest_num > 0:
        last_line = np.zeros((images.shape[1],height,width*rest_num))

        for j in range(rest_num):
            last_line[:,:,j*width:(j+1)*width] = images[h_num*line_num + j]

        if line_num > 0:
            white = np.zeros((images.shape[1],height,width*(h_num-rest_num)))
            last_line = np.concatenate([last_line,white],axis=-1)

        image_data.append(last_line)
    n_image = np.concatenate(image_data,axis=1)
    n_image = n_image.transpose((1,2,0))
    # cv2.imwrite('test1.png', n_image[:,:,0])

    isize = (int(n_image.shape[1]*factor), int(n_image.shape[0]*factor))
    n_image = cv2.resize(n_image,isize)
    if len(n_image.shape) == 2:
        n_image = np.expand_dims(n_image,2)
    # cv2.imwrite('test2.png',n_image)
    if out_type == 'CHW':
        n_image = n_image.transpose((2,0,1))
    n_image = n_image.astype('uint8')
    return n_image

def crop_image_black(img, move_step = 2):
    tmp_img = img
    if len(img.shape) == 3:
        tmp_img = img[0]

    img_size = (tmp_img.shape[1], tmp_img.shape[0])
    center_point = (tmp_img.shape[1] // 2, tmp_img.shape[0] // 2)

    width_range = []
    for turn in [-1,1]:
        cur_point = center_point[0]
        while cur_point > 0 and cur_point < img_size[0]:
            tmp_list = tmp_img[center_point[1] - 20: center_point[1] + 20, cur_point]
            if (tmp_list < 0.5).astype('uint8') / tmp_list.shape[0] > 0.5:
                width_range.append(cur_point)
                break
            cur_point += turn * move_step

        if cur_point < 0:
            cur_point = 0
            width_range.append(cur_point)

        if cur_point > img_size[0]:
            cur_point = img_size[0]
            width_range.append(cur_point)

    height_range = []
    for turn in [-1,1]:
        cur_point = center_point[1]
        while cur_point > 0 and cur_point < img_size[1]:
            tmp_list = tmp_img[cur_point, center_point[0] - 20: center_point[0] + 20]
            if (tmp_list < 0.5).astype('uint8') / tmp_list.shape[0] > 0.5:
                height_range.append(cur_point)
                break
            cur_point += turn * move_step

        if cur_point < 0:
            cur_point = 0
            height_range.append(cur_point)

        if cur_point > img_size[1]:
            cur_point = img_size[1]
            height_range.append(cur_point)

    return img[height_range[0]:height_range[1], width_range[0]:width_range[1]]

def save_image(image_numpy, image_path, aspect_ratio=1.0):
    """Save a numpy image to the disk

    Parameters:
        image_numpy (numpy array) -- input numpy array
        image_path (str)          -- the path of the image
    """

    image_pil = Image.fromarray(image_numpy)
    h, w, _ = image_numpy.shape

    if aspect_ratio > 1.0:
        image_pil = image_pil.resize((h, int(w * aspect_ratio)), Image.BICUBIC)
    if aspect_ratio < 1.0:
        image_pil = image_pil.resize((int(h / aspect_ratio), w), Image.BICUBIC)
    image_pil.save(image_path)

def save_debug_image(img, img_dir, norm2_255 = False, ap = '', max_num = -1):
    if isinstance(img, torch.Tensor):
        img = img.numpy()
    mkdir(img_dir)
    img_num = len(os.listdir(img_dir))
    if max_num >= 0 and img_num >= max_num:
        return

    if norm2_255:
        img = img - np.min(img).item()
        img *= (255 / np.max(img).item())
    p = os.path.join(img_dir, ap + str(img_num) + '.jpg')
    cv2.imwrite(p, img.astype('uint8'))
    if img_num % 100 == 0:
        print('save', p)

def save_typ_images(img_dict, save_dir, norm2_255 = False, name=''):
    """
    input:
        img_dict: dict of list of tuple(image score, image tensor)
        save_dir: root dir of the saved image(/vis/name/)
        name: current epoch counter (epochx)
    """
    for key, value in img_dict.items():
        save_name = name + '_' + str(key)
        num = len(value)
        slice_num = (num // 10) if num % 10 == 0 else (num // 10 + 1)
        for i in range(slice_num):
            slice_name = save_name + str(i) + '.jpg'
            img_path= os.path.join(save_dir, slice_name)
            for p in range(10):
                idx = i * 10 + p
                if idx >= num:
                    break
                img = value[i * 10 + p][1]
                if isinstance(img, torch.Tensor):
                    img = img.numpy()
                if norm2_255:
                    img = img - np.min(img).item()
                    img *= (255 / np.max(img).item())

                if len(img.shape) == 3:
                    img = img.transpose(1, 2, 0)
                elif len(img.shape) == 4:
                    img = img.transpose(0, 2, 3, 1)
                if p == 0:
                    cimg = copy.deepcopy(img)
                else:
                    cimg = np.concatenate([cimg, img], axis=1)
            print('saving %s' % img_path)
            cv2.imwrite(img_path, cimg.astype('uint8'))
    #
    #
    # label = err_list[0][0]
    # if label == 1:
    #     p = os.path.join(save_dir, 'epoch' + name + '_abnormal.jpg')
    # else:
    #     p = os.path.join(save_dir, 'epoch' + name + '_normal.jpg')
    # for i, sample in enumerate(err_list):
    #     img = sample[1]
    #
    #     if isinstance(img, torch.Tensor):
    #         img = img.numpy()
    #     if norm2_255:
    #         img = img - np.min(img).item()
    #         img *= (255 / np.max(img).item())
    #
    #     if len(img.shape) == 3:
    #         img = img.transpose(1, 2, 0)
    #     elif len(img.shape) == 4:
    #         img = img.transpose(0, 2, 3, 1)
    #     if i == 0:
    #         comp_img = img
    #     else:
    #         comp_img = np.concatenate([comp_img, img], axis=1)
    #
    # cv2.imwrite(p, comp_img.astype('uint8'))
    # print('save %s' % p)

def save_hist(scores_dict, vis_dir='', save_name=''):
    mkdir(vis_dir)
    normal_scores = scores_dict['normal']
    abnormal_scores = scores_dict['abnormal']

    fig, axs = plt.subplots(nrows=3, ncols=1, figsize=(24, 24))
    kwargs = dict(range=(0, 1), bins=20, density=True, histtype='stepfilled')
    tmp_label0 = 'normal (total num = %i)' % len(normal_scores)
    tmp_label1 = 'abnormal (total num = %i)' % len(abnormal_scores)
    axs[0].hist(normal_scores, label=tmp_label0, **kwargs)
    axs[0].set_xlabel('Distance to center')
    axs[0].set_ylabel('Probability density')
    axs[0].set_title('Histogram of normal samples distance')
    axs[0].legend()

    axs[1].hist(abnormal_scores, label=tmp_label1, **kwargs)
    axs[1].set_xlabel('Distance to center')
    axs[1].set_ylabel('Probability density')
    axs[1].set_title('Histogram of abnormal samples distance')
    axs[1].legend()

    axs[2].hist(normal_scores, label=tmp_label0, alpha=0.3, **kwargs)
    axs[2].hist(abnormal_scores, label=tmp_label1, alpha=0.3, **kwargs)
    axs[2].set_xlabel('Distance to center')
    axs[2].set_ylabel('Probability density')
    axs[2].set_title('Histogram of samples distance')
    axs[2].legend()

    save_path = os.path.join(vis_dir, save_name)
    plt.savefig(save_path, bbox_inches='tight')
    plt.close()

def save_disb_image(suffix, img_dict, save_dir, view_overlap=False, norm2_255=False):
    mkdir(save_dir)
    normal_score = img_dict['normal_score']
    abnormal_score = img_dict['abnormal_score']

    fig, axs = plt.subplots(nrows=3, ncols=1, figsize=(54, 27))
    # fig, axs = plt.subplots(nrows=3, ncols=1, figsize=(54, 54))

    axs[0].scatter(normal_score, [1] * len(normal_score), c='dodgerblue', label='normal')
    axs[0].scatter(abnormal_score, [2] * len(abnormal_score), c='orange', label='abnormal')
    axs[0].set_ylim(0, 3)
    axs[0].yaxis.grid(True)
    axs[0].set_xticks([])
    axs[0].set_yticks([1, 2])
    axs[0].set_yticklabels(['normal', 'abnormal'])
    axs[0].set_xlabel(suffix + ' score of scan')
    axs[0].set_title(suffix + ' score distribution - scatter')
    axs[0].legend()

    box_x = [normal_score, abnormal_score]
    axs[1].boxplot(box_x, vert=False, showmeans=True)
    axs[1].set_yticks([1, 2])
    axs[1].set_yticklabels(['normal', 'abnormal'])
    axs[1].set_ylim(0, 3)
    axs[1].set_xticks([])
    axs[1].set_xlabel(suffix + ' score of scan')
    axs[1].set_title(suffix + ' score distribution - box')


    kwargs = dict(range=(0,1), bins=100, density=True, histtype='stepfilled', alpha=0.3)
    tmp_label0 = 'normal (total num = %i)' % len(normal_score)
    tmp_label1 = 'abnormal (total num = %i)' % len(abnormal_score)
    axs[2].hist(normal_score, label=tmp_label0, **kwargs)
    axs[2].hist(abnormal_score, label=tmp_label1, **kwargs)
    axs[2].set_xlabel(suffix + ' score of scan')
    axs[2].set_ylabel('Probability density')
    axs[2].set_title('Histogram of ' + suffix + ' score')
    axs[2].legend()

    save_path = os.path.join(save_dir, 'ScoreDistribution_' + suffix + '.jpg')
    plt.savefig(save_path, bbox_inches='tight')
    print('saving the %s score distribution to %s' % (suffix, save_path))
    plt.close()

import seaborn as sns

sns.set_style('darkgrid')
sns.set_palette('muted')
sns.set_context("notebook", font_scale=1.5,
                rc={"lines.linewidth": 2.5})

def save_tsne(data, label, disease_name, vis_dir='', save_name='', title=''):
    # We choose a color palette with seaborn.
    mkdir(vis_dir)
    palette = np.array(sns.color_palette("hls", 2))

    neg_data = []
    pos_data = []
    for i in range(len(label)):
        if label[i] == 0:
            neg_data.append(data[i, ...])
        else:
            pos_data.append(data[i, ...])
    neg_data = np.stack(neg_data, axis=0)
    pos_data = np.stack(pos_data, axis=0)

    # Create a scatter plot.
    plt.figure(figsize=(10, 10))
    plt.subplot(aspect='equal')
    plt.scatter(neg_data[:, 0], neg_data[:, 1], color=palette[1], alpha=0.8, label='normal')
    plt.scatter(pos_data[:, 0], pos_data[:, 1], color=palette[0], alpha=0.8, label=disease_name)

    plt.xticks([])
    plt.yticks([])
    plt.axis('tight')
    plt.legend()
    plt.xlabel('z1')
    plt.ylabel('z2')
    plt.title(title)
    save_path = os.path.join(vis_dir, save_name)
    plt.savefig(save_path, dpi=120)


def scatter2d(x, colors, only_source=0, only_target=0, vis_dir='', save_name='', target='', debug=False, with_center=False, title=''):
    # We choose a color palette with seaborn.
    mkdir(vis_dir)
    palette = np.array(sns.color_palette("hls", 4))
    data = x

    data0 = []
    data1 = []
    data2 = []
    data3 = []
    for i in range(len(colors)):
        if colors[i] == 0:
            data0.append(data[i, ...])
        elif colors[i] == 1:
            data1.append(data[i, ...])
        elif colors[i] == 2:
            data2.append(data[i, ...])
        else:
            data3.append(data[i, ...])

    if len(data0) > 0:
        data0 = np.stack(data0, axis=0)
    if len(data1) > 0:
        data1 = np.stack(data1, axis=0)
    if len(data2) > 0:
        data2 = np.stack(data2, axis=0)
    if len(data3) > 0:
        data3 = np.stack(data3, axis=0)

    # We create a scatter plot.
    plt.figure(figsize=(10, 10))
    plt.subplot(aspect='equal')
    # for i in range(len(colors)):
    #     plt.scatter(data[i, 0], data[i, 1], color=palette[colors[i]], alpha=0.8, label=str(colors[i]))
    if not only_target:
       plt.scatter(data0[:, 0], data0[:, 1], color=palette[1], alpha=0.8, label='source_negative')
       plt.scatter(data1[:, 0], data1[:, 1], color=palette[0], alpha=0.8, label='source_positive')
    if not only_source:
       plt.scatter(data2[:, 0], data2[:, 1], color=palette[2], alpha=0.8, label='target_negative')
       plt.scatter(data3[:, 0], data3[:, 1], color=palette[3], alpha=0.8, label='target_positive')


    # plt.scatter(x[:, 0], x[:, 1], lw=0, s=40, c=palette[colors.astype(np.int)])
    # plt.xlim(-25, 25)
    # plt.ylim(-25, 25)
    # plt.axis('off')
    plt.tick_params(labelsize=2)
    plt.axis('tight')
    plt.legend()
    plt.xlabel('feature 1')
    plt.ylabel('feature 2')
    plt.title(title)
    if save_name != '':
        save_path = os.path.join(vis_dir, save_name)
    else:
        save_path = os.path.join(vis_dir, 'target' + str(target) + '_tsne2d.png')
    plt.savefig(save_path, dpi=120)