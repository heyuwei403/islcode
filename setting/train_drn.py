from setting import *
av_gpus = '0,1,2,3,4,5,6,7'
memory = 10
gpu_num = 1
start_time = 600

add_params('dataset_mode', ['drn'])
add_params('v_dataset_mode', ['drn'])
add_params('model', ['drn'])
add_params('net_type', ['resnet'])
add_params('backbone', ['resnet18'])
add_params('use_post', [0])
add_params('in_channels', [3])
add_params('no_normalize', [0])
add_params('temperature', [0.1])
add_params('v_metric', ['auc_slice'])

vis_num = no_vis_show(params_list)

add_params('early_stop', [0])
add_params('load_size', [256])
add_params('batch_size v_batch_size', [(4, 1)])
add_params('niter niter_decay', [(50, 0)])
add_params('lr_policy', ['cosine'])
add_params('lr', [0.001])
add_params('print_freq', [500])
add_params('v_print_freq', [500])


params['stage' + str(stage_ind)]['params'] = params_list
params['stage' + str(stage_ind)]['values'] = value_matrix
params['stage' + str(stage_ind)]['vis_num'] = vis_num
params['stage' + str(stage_ind)]['memory'] = memory
params['stage' + str(stage_ind)]['av_gpus'] = av_gpus
params['stage' + str(stage_ind)]['gpu_num'] = gpu_num
params['stage' + str(stage_ind)]['start_time'] = start_time
