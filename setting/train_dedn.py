from setting import *
av_gpus = '4,5,6,7'
# av_gpus = '4'
memory = 10
gpu_num = 1
start_time = 600

add_params('dataset_mode', ['dedn'])
add_params('v_dataset_mode', ['dedn'])
add_params('model', ['dedn'])

vis_num = no_vis_show(params_list)

add_params('load_size', [256])
add_params('batch_size v_batch_size', [(4, 1)])


params['stage' + str(stage_ind)]['params'] = params_list
params['stage' + str(stage_ind)]['values'] = value_matrix
params['stage' + str(stage_ind)]['vis_num'] = vis_num
params['stage' + str(stage_ind)]['memory'] = memory
params['stage' + str(stage_ind)]['av_gpus'] = av_gpus
params['stage' + str(stage_ind)]['gpu_num'] = gpu_num
params['stage' + str(stage_ind)]['start_time'] = start_time
