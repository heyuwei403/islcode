import torch
import torch.nn as nn
import torch.nn.functional as F
from .torchnets import resnet as resnet
from .layers.identity_layers import IdentityLayer
from .torchnets import vgg as vgg

resnet_dict = {"resnet18": resnet.resnet18, "resnet34":resnet.resnet34, "resnet50":resnet.resnet50, "resnet101":resnet.resnet101, "resnet152":resnet.resnet152}
vgg_dict = {'vgg11': vgg.vgg11, 'vgg11_bn': vgg.vgg11_bn, 'vgg13': vgg.vgg13, 'vgg13_bn': vgg.vgg13_bn,
            'vgg16': vgg.vgg16, 'vgg16_bn': vgg.vgg16_bn, 'vgg19': vgg.vgg19, 'vgg19_bn': vgg.vgg19_bn}
resnet_infeature_dict = {"resnet18": 512, "resnet34": 512, "resnet50": 2048, "resnet101": 2048, "resnet152": 2048}

def init_weights(m):
    classname = m.__class__.__name__
    if classname.find('Conv2d') != -1 or classname.find('ConvTranspose2d') != -1:
        nn.init.kaiming_uniform_(m.weight)
        nn.init.zeros_(m.bias)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(m.weight, 1.0, 0.02)
        nn.init.zeros_(m.bias)
    elif classname.find('Linear') != -1:
        nn.init.xavier_normal_(m.weight)
        if hasattr(m, 'bias') and m.bias is not None:
            nn.init.constant_(m.bias.data, 0.0)
        # nn.init.zeros_(m.bias)

class FlattenLayer(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, x):
        x = x.view(x.size(0), -1)
        return x

class BaseNetwork(nn.Module):
    def __init__(self):
        super(BaseNetwork, self).__init__()

    def init_weights(self, init_type='normal', gain=0.02):
        '''
        initialize network's weights
        init_type: normal | xavier | kaiming | orthogonal
        https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/9451e70673400885567d08a9e97ade2524c700d0/models/networks.py#L39
        '''

        def init_func(m):
            classname = m.__class__.__name__
            if hasattr(m, 'weight') and (classname.find('Conv') != -1 or classname.find('Linear') != -1):
                if init_type == 'normal':
                    nn.init.normal_(m.weight.data, 0.0, gain)
                elif init_type == 'xavier':
                    nn.init.xavier_normal_(m.weight.data, gain=gain)
                elif init_type == 'kaiming':
                    nn.init.kaiming_normal_(m.weight.data, a=0, mode='fan_in')
                elif init_type == 'orthogonal':
                    nn.init.orthogonal_(m.weight.data, gain=gain)

                if hasattr(m, 'bias') and m.bias is not None:
                    nn.init.constant_(m.bias.data, 0.0)

            elif classname.find('BatchNorm2d') != -1:
                nn.init.normal_(m.weight.data, 1.0, gain)
                nn.init.constant_(m.bias.data, 0.0)

        self.apply(init_func)

class DeepSVDD(BaseNetwork):
    def __init__(self, in_channels=3, rep_dim=512, with_fc=True, init=True, init_type='normal'):
        super(DeepSVDD, self).__init__()
        self.rep_dims = rep_dim
        self.with_fc = with_fc
        # self.pool = nn.MaxPool2d(2, 2)

        self.conv1 = nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=64, kernel_size=5, stride=2, padding=1, bias=False),
            nn.LeakyReLU(0.2, inplace=True),
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(in_channels=64, out_channels=128, kernel_size=5, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(num_features=128),
            nn.LeakyReLU(0.2, inplace=True),
        )
        self.conv3 = nn.Sequential(
            nn.Conv2d(in_channels=128, out_channels=256, kernel_size=5, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(num_features=256),
            nn.LeakyReLU(0.2, inplace=True),
        )
        self.conv4 = nn.Sequential(
            nn.Conv2d(in_channels=256, out_channels=512, kernel_size=5, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(num_features=512),
            nn.LeakyReLU(0.2, inplace=True),
        )
        self.backbone = nn.Sequential(self.conv1, self.conv2, self.conv3, self.conv4)
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        if self.with_fc:
            self.fc = nn.Sequential(
                # nn.Linear(512 * 15 * 15, rep_dim, bias=False),
                nn.Linear(512, rep_dim, bias=False),
            )
        if init:
            self.init_weights(init_type=init_type)

    def forward(self, x):
        conv4 = self.backbone(x)
        conv4 = self.avgpool(conv4)
        f = conv4.view(conv4.size(0), -1)  # [bs, 512]
        if self.with_fc:
            f = self.fc(f)  # [bs, rep_dim]
        return f, f

class ResNet(nn.Module):

    def __init__(self, in_channels=3, backbone='resnet50', rep_dim=512, with_fc=True, norm_method='batch', **kwargs):
        super(ResNet,self).__init__()  # call the initialization method of BaseModel
        model_resnet = resnet_dict[backbone](pretrained=True, norm_type=norm_method)
        org_weight = model_resnet.conv1.weight  # [64,3,7,7]
        new_weight = 0.299 * org_weight[:, :1, ...] + 0.587 * org_weight[:, 1:2, ...] + 0.114 * org_weight[:, 2:3, ...]
        model_resnet.conv1 = nn.Conv2d(in_channels, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
        model_resnet.conv1.weight = nn.Parameter(torch.cat([new_weight] * in_channels, dim=1))

        self.with_fc = with_fc
        self.rep_dim = rep_dim
        self.in_features = resnet_infeature_dict[backbone]
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1)) #自适应pooling

        self.conv1 = model_resnet.conv1
        self.bn1 = model_resnet.bn1
        self.relu = model_resnet.relu
        self.maxpool = model_resnet.maxpool
        self.layer1 = model_resnet.layer1
        self.layer2 = model_resnet.layer2
        self.layer3 = model_resnet.layer3
        self.layer4 = model_resnet.layer4


        self.backbone = nn.Sequential(self.conv1, self.bn1, self.relu, self.maxpool, self.layer1, self.layer2,
                                      self.layer3, self.layer4, self.avgpool)
        if self.with_fc:
            if self.in_features // 2 < self.rep_dim or ('fc_layers' in kwargs.keys() and kwargs['fc_layers'] == 1):
                print('one class network structure changed. FC layers reduced to one.')
                self.fc = nn.Linear(self.in_features, self.rep_dim, bias=False)
            else:
                self.fc = nn.Sequential(
                    nn.Linear(self.in_features, self.in_features // 2, bias=False),
                    nn.ReLU(inplace=True),
                    nn.Linear(self.in_features // 2, self.rep_dim, bias=False)
                )
            self.fc.apply(init_weights)
            self.__in_features = rep_dim

    def forward(self, input):
        x = self.backbone(input)
        x = x.view(x.size(0), -1)  # resnet18: [bs, 512]; resnet50: [bs, 2048]
        if self.with_fc:
            x = self.fc(x)  # [bs, rep_dim]
        return x, x

class Discriminator_fc(BaseNetwork):
    def __init__(self, in_channels, use_sigmoid=True, use_spectral_norm=True, init_weights=True):
        super(Discriminator_fc, self).__init__()
        self.use_sigmoid = use_sigmoid

        self.conv1 = self.features = nn.Sequential(
            spectral_norm(nn.Conv2d(in_channels=in_channels, out_channels=64, kernel_size=4, stride=2, padding=1, bias=not use_spectral_norm), use_spectral_norm),
            nn.LeakyReLU(0.2, inplace=True),
        )

        self.conv2 = nn.Sequential(
            spectral_norm(nn.Conv2d(in_channels=64, out_channels=128, kernel_size=4, stride=2, padding=1, bias=not use_spectral_norm), use_spectral_norm),
            nn.LeakyReLU(0.2, inplace=True),
        )

        self.conv3 = nn.Sequential(
            spectral_norm(nn.Conv2d(in_channels=128, out_channels=256, kernel_size=4, stride=2, padding=1, bias=not use_spectral_norm), use_spectral_norm),
            nn.LeakyReLU(0.2, inplace=True),
        )

        self.conv4 = nn.Sequential(
            spectral_norm(nn.Conv2d(in_channels=256, out_channels=512, kernel_size=4, stride=1, padding=1, bias=not use_spectral_norm), use_spectral_norm),
            nn.LeakyReLU(0.2, inplace=True),
        )

        self.conv5 = nn.Sequential(
            spectral_norm(nn.Conv2d(in_channels=512, out_channels=1, kernel_size=4, stride=1, padding=1, bias=not use_spectral_norm), use_spectral_norm),
        )

        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))  # 自适应pooling
        # in_features = 30*30*1
        self.projector = nn.Sequential(nn.Linear(30*30*1, 1, bias=False))

        if init_weights:
            self.init_weights()

    def forward(self, x):
        conv1 = self.conv1(x)
        conv2 = self.conv2(conv1)
        conv3 = self.conv3(conv2)
        conv4 = self.conv4(conv3)
        conv5 = self.conv5(conv4)
        # print('conv5:', conv5.shape)
        xavp = conv5
        # xavp = self.avgpool(conv5)
        # print('xavp:', xavp.shape)
        h = xavp.view(xavp.size(0), -1)
        # print('feature shape:', h.shape) # [bs, 900]
        outputs = self.projector(h)
        # print('outputs:', outputs.shape)

        if self.use_sigmoid:
            outputs = torch.sigmoid(outputs)

        return outputs, h

def spectral_norm(module, mode=True):
    if mode:
        return nn.utils.spectral_norm(module)

    return module
