
import torch
from .base_model import BaseModel
from .networks.inpaint_net import InpaintGenerator
from .networks.oneclass_net import ResNet
from util import *
from util.basic import *
from util import metrics
from util.augmentation2 import *
import torch.nn as nn
import torch.nn.functional as F
from util.optimizer.adabound import AdaBound
from util.model import combine_with_id
from schedulers import get_scheduler
import cv2


class DRNModel(BaseModel):
    @staticmethod
    def modify_commandline_options(parser):
        # options for the inpaint net
        parser.add_argument('--edge_mask_num', type=int, default=4)
        parser.add_argument('--load_edge_num', type=int, default=4)
        parser.add_argument('--multi_edge', type=int, default=1)
        parser.add_argument('--gen_net_type', type=str, default='inpaint')
        parser.add_argument('--inpaint_dir_ind', type=int, default=1)
        parser.add_argument('--dedn_load_dir', type=str, default='')
        parser.add_argument('--center_load_dir', type=str, default='')
        parser.add_argument('--center_dir_ind', type=int, default=-1)

        # options for the occ net
        parser.add_argument('--load_moco', type=int, default=1)
        parser.add_argument('--in_channels', type=int, default=1)
        parser.add_argument('--net_type', type=str, default='custom', choices=['custom', 'svdd', 'resnet'])
        parser.add_argument('--backbone', type=str, default='resnet50')
        parser.add_argument('--fc_layer_num', type=int, default=2)
        parser.add_argument('--rep_dim', type=int, default=64)
        parser.add_argument('--valid_mode', type=str, default='net', choices=['net', 'value'])
        parser.add_argument('--v_metric', type=str, default='')
        parser.add_argument('--no_normalize', type=int, default=1)
        parser.add_argument('--use_post', type=int, default=0)
        parser.add_argument('--use_org', type=int, default=0)
        parser.add_argument('--temperature', type=float, default=0.1)
        parser.add_argument('--dis_weight', type=float, default=1)
        parser.add_argument('--compact_weight', type=float, default=1)
        parser.add_argument('--collapse_weight', type=float, default=1)


        # options for computing the difference image and difference value
        parser.add_argument('--l_exp_ratio', type=float, default=16)
        parser.add_argument('--h_exp_ratio', type=float, default=64)
        parser.add_argument('--bg_width', type=int, default=2)
        parser.add_argument('--cut_thred', type=float, default=-1)

        parser.add_argument('--l_pixel_thred_low', type=float, default=3)
        parser.add_argument('--l_pixel_thred_high', type=float, default=150)
        parser.add_argument('--h_pixel_thred_low', type=float, default=15)
        parser.add_argument('--h_pixel_thred_high', type=float, default=200)


        # others
        parser.add_argument('--vis_ref', type=int, default=0)
        parser.add_argument('--save_pred_score', type=int, default=1)
        parser.add_argument('--vis_cluster', type=int, default=0)
        parser.add_argument('--vis_slice', type=int, default=0)
        parser.add_argument('--vis_scan', type=int, default=0)
        parser.add_argument('--vis_debug', type=int, default=0)
        parser.add_argument('--debug_res_image', type=int, default=0)
        parser.add_argument('--debug_scan_res', type=int, default=0)
        parser.add_argument('--debug_bone', type=int, default=0)
        parser.add_argument('--mn', type=int, default=-1)

        return parser

    def __init__(self, opt):
        super().__init__(opt)

        self.buffer_glabels = []
        self.buffer_gscores = []
        self.buffer_glscores = []
        self.buffer_ghscores = []
        self.buffer_gpreds = []
        self.buffer_ginput_ids = []
        self.buffer_gfeatures = []
        self.buffer_gscan_ids = []

        self.buffer_scanlabels = []
        self.buffer_scanpreds = []
        self.buffer_scanscores = []
        self.buffer_scanlscores = []
        self.buffer_scanhscores = []
        self.buffer_scanfeatures = []
        self.buffer_scanpaths = []

        '''The parameters to be reset for a new model'''

        self.loss_names = ['compact', 'collapse', 'c'] # used to update networks,
        self.s_metric_names = [] # scalar metric, stat local information
        self.g_metric_names = ['auc', 'auc_slice', 'auc_pscore', 'sensitivity', 'specificity', 'precision', 'recall', 'ap'] # scalar metric, stat global information
        self.t_metric_names = ['cmatrix'] # table or matrix metric
        self.net_names = ['discriminator']
        self.optimizers = []


        self.valid_metric = 'auc'
        self.scheduler_metric = 'auc'
        if self.opt.v_metric != '':
            self.valid_metric = self.opt.v_metric
            self.scheduler_metric = self.opt.v_metric
        self.valid_metric_dir = 1
        self.scheduler_metric_dir = 1

        print('Reconstruction net type: ', self.opt.gen_net_type)

        self.center = None

        self.generator = self.prepare_gen_net().cuda()
        self.in_channels = self.opt.in_channels
        self.net_discriminator = ResNet(in_channels=self.in_channels, backbone=opt.backbone, rep_dim=opt.rep_dim,
                                        with_fc=True, fc_layers=opt.fc_layer_num)

        self.bce_loss = nn.BCELoss(reduction='none').cuda()
        self.l2_loss = nn.MSELoss(reduction='none').cuda()

        self.bg_conv_weight = torch.ones(1, 1, 2 * self.opt.bg_width + 1, 2 * self.opt.bg_width + 1).cuda()
        self.temperature = self.opt.temperature
        self.canny_list = [(0, 0, 0), (15, 20, 100), (5, 200, 240)]
        self.scan_dict = {}

    @staticmethod
    def set_train_load_dir(opt):
        if opt.load_dir != '':
            return

        if opt.load_dir_ind < 0:
            return

        load_dict = {
            0:'',
        }
        opt.load_dir = load_dict[opt.load_dir_ind]

    @staticmethod
    def set_valid_load_dir(opt):
        if opt.load_dir != '':
            return

        if opt.load_dir_ind < 0:
            return

        load_dict = {
            0: '',
        }
        opt.load_dir = load_dict[opt.load_dir_ind]

    @staticmethod
    def set_dedn_load_dir(opt):
        if opt.dedn_load_dir != '':
            return

        if opt.inpaint_dir_ind < 0:
            return

        load_dict = {
            0: '',
        }
        opt.dedn_load_dir = load_dict[opt.inpaint_dir_ind]
        load_params = ['multi_ww', 'with_naoshi_info', 'lunkuo_num', 'edge_mask_num', 'mask_num', 'load_edge_num']
        items = opt.dedn_load_dir.split(',')
        for param in load_params:
            for item in items:
                if item.startswith(param + '='):
                    v = item[len(param + '='):]
                    v = int(v)
                    setattr(opt, param, v)
                    print('set parameter:' + param + ' from load dir.')
                    break
        if opt.inpaint_dir_ind == 1:
            setattr(opt, 'mask_num', 6)
            print('set parameter: mask_num from load dir.')
        elif opt.inpaint_dir_ind == 2:
            setattr(opt, 'mask_num', 8)
            print('set parameter: mask_num from load dir.')
        else:
            pass

    @staticmethod
    def supply_option_info(opt):
        if opt.l_state == 'train':
            ContrastV2OCTModel.set_train_load_dir(opt)
        else:
            ContrastV2OCTModel.set_valid_load_dir(opt)
        ContrastV2OCTModel.set_dedn_load_dir(opt)
        return opt

    def setup(self, opt):
        """Load and print networks; create schedulers

        Parameters:
            opt (Option class) -- stores all the experiment flags; needs to be a subclass of BaseOptions
        """

        self.buffer_names = self.get_buffer_names()
        self.meters = self.gen_meters()
        self.schedulers = []

        if opt.net_type == 'resnet' and opt.load_moco:
            moco_dict = {
                'resnet50': '/home/maliangdi/oneclass/moco/checkpoint_0003.pth.tar',
                'resnet18': '/home/maliangdi/oneclass/moco/ct_checkpoint_0002.pth.tar',
            }
            self.load_networks('moco', load_dir=moco_dict[opt.backbone], strict=self.opt.load_strict)

        if opt.load_dir != '' and opt.load_net:
            load_suffix = 'optimal'
            self.load_networks(load_suffix, load_dir=opt.load_dir, strict=self.opt.load_strict)
            self.print_networks(opt.verbose)

        self.change_net_structure()

        if opt.l_state == 'train':
            self.set_optimizer(opt)
            self.schedulers = [get_scheduler(optimizer, opt) for optimizer in self.optimizers]

        if self.opt.save_base_model:
            for name in self.net_names:
                if isinstance(name, str):
                    save_filename = '%s_net_%s.pth' % ('base', name)
                    save_path = os.path.join(self.save_dir, save_filename)
                    net = getattr(self, 'net_' + name)
                    torch.save(net.state_dict(), save_path)

        net_names = self.net_names

        # visualization
        if opt.vis_method is not None and opt.vis_method != '':
            self.vis_method = vis_method_dict[opt.vis_method](self)

        for name in net_names:
            net = getattr(self,'net_' + name)
            net.cuda(self.gpu_ids[0])
            if len(self.gpu_ids) > 1:
                setattr(self,'net_' + name,nn.DataParallel(net,opt.gpu_ids))

        if self.opt.l_state == 'train':
            self.train()
        else:
            self.eval()

    def set_optimizer(self, opt):
        for name in self.net_names:
            tmp_net = getattr(self, 'net_' + name)
            if opt.op_name == 'SGD':
                print('The optimizer is SGD')
                tmp_optimizer = torch.optim.SGD(tmp_net.parameters(), lr=opt.lr, momentum=opt.momentum,
                                                nesterov=opt.nesterov, weight_decay=opt.weight_decay)
            elif opt.op_name == 'Adam':
                print('The optimizer is Adam')
                tmp_optimizer = torch.optim.Adam(tmp_net.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999),
                                                         weight_decay=opt.weight_decay)
            elif opt.op_name.lower() == 'adabound':
                print('The optimizer is adabound')
                tmp_optimizer = AdaBound(tmp_net.parameters(), lr=opt.lr, final_lr=opt.final_lr)
            else:
                assert False, 'unrecognized optimizer type.'

            setattr(self, 'optimizer_' + name, tmp_optimizer)
            self.optimizers.append(tmp_optimizer)

    def set_input(self, data):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.

        Parameters:
            input (dict): includes the data itself and its metadata information.
        """
        self.input, _, self.edges, self.masks, _, self.label, self.input_id, self.scan_id, self.slice_id = data
        self.input = self.input.cuda()
        self.edges = self.edges.cuda()
        self.masks = self.masks.cuda()
        self.label = self.label.cuda()
        self.input_size = self.input.shape[0]
        self.input_shape = self.input.shape

        if self.opt.l_state == 'train':
            self.label = torch.where(self.label == 2, torch.zeros_like(self.label).cuda(), self.label)

        self.scan_list = list(self.scan_id)
        self.slice_id = list(self.slice_id)

        self.or_input = self.input
        new_shape = [self.input_shape[0] * self.input_shape[1]] + list(self.input_shape)[2:]
        self.input = self.input.view(new_shape)

        self.edges_shape = self.edges.shape
        new_shape = [self.input_shape[0] * self.input_shape[1]] + list(self.edges_shape)[2:]
        self.edges = self.edges.view(new_shape)

        self.masks_shape = self.masks.shape
        new_shape = [self.input_shape[0] * self.input_shape[1]] + list(self.masks_shape)[2:]
        self.masks = self.masks.view(new_shape)

    def set_center(self, opt, dataset=None):
        load_dict = {
            0: '',
        }
        if opt.l_state == 'train':
            if opt.center_load_dir != '' or opt.center_dir_ind >= 0:
                if opt.center_load_dir == '':
                    opt.center_load_dir = load_dict[opt.center_dir_ind]
                self.center = self.load_center()
            else:
                assert dataset != None, 'No dataset is given for calculate feature center.'
                self.center = self.cal_center(dataset)
            self.save_center()
        else:
            if opt.center_load_dir != '':
                pass
            elif opt.center_dir_ind >= 0:
                opt.center_load_dir = load_dict[opt.center_dir_ind]
            else:
                opt.center_load_dir = opt.load_dir

            self.center = self.load_center()

    def load_center(self):
        load_filename = 'center.pth'
        load_path = os.path.join(self.opt.center_load_dir, load_filename)

        if not osp.exists(load_path):
            print('parameter "center" has no state dict')
            return

        hp_dict = torch.load(load_path)
        print('loading the parameter "center" from %s' % (load_path))

        return hp_dict['center']

    def cal_center(self, dataset):
        gcenter = []
        self.eval()
        o_l_state = self.opt.l_state
        self.opt.l_state = 'valid'

        neg_num = 0
        with tqdm.tqdm(total=len(dataset)) as pbar:
            for i, data in enumerate(dataset):
                self.set_input(data)
                labels = self.label.tolist()
                neg_ind = [k for k in range(self.input_size) if labels[k] == 0]
                neg_num += len(neg_ind)

                if len(neg_ind) > 0:
                    with torch.no_grad():
                        gen_outputs = self.gen_images()
                        self.org_image = torch.cat([self.or_input[:, 0, :1, ...]] * 3, dim=1)
                        self.rec_image = torch.cat([gen_outputs[:, :1, ...]] * 3, dim=1)
                        self.res_image = self.gen_res_images(self.org_image, self.rec_image)
                        self.gen_net_input()

                        _, self.output_h = self.net_discriminator(self.oc_input)  # [N, d]
                        if not self.opt.no_normalize:
                            self.output_h = F.normalize(self.output_h, p=2, dim=1)
                        tmp_center = torch.sum(self.output_h[neg_ind], dim=0, keepdim=True)  # [1,d]
                        gcenter.append(tmp_center)
                pbar.update(self.input_size)
        gcenter = torch.cat(gcenter, dim=0) #[k, d]
        center = torch.sum(gcenter, dim=0, keepdim=True) #[1,d]
        center = center / neg_num
        self.train()
        self.opt.l_state = o_l_state
        return center

    def save_center(self):
        hyp_dict = {}
        save_filename = 'center.pth'
        save_path = os.path.join(self.save_dir, save_filename)
        hyp_value = getattr(self, 'center')
        hyp_dict.update({'center': hyp_value})
        if os.path.exists(save_path):
            print('center already existed in the dir:', save_path)
        else:
            torch.save(hyp_dict, save_path)

    def gen_images(self):
        images = self.input
        edges = self.edges
        masks = self.masks
        images_masked = (images * (1 - masks).float()) + masks
        if self.opt.edge_mask_num > 0:
            inputs = torch.cat((images_masked, edges), dim=1)
        else:
            inputs = images_masked

        with torch.no_grad():
            outputs = self.generator(inputs)                                    # in: [rgb(3) + edge(1)]
        outputs = (outputs * masks).view(self.input_shape)
        outputs_merged = outputs.sum(dim = 1)
        return outputs_merged

    def prepare_gen_net(self):
        self.set_dedn_load_dir(self.opt)
        load_path = os.path.join(self.opt.dedn_load_dir,'optimal_net_generator.pth')
        print('load generator from', load_path)

        # in_channel = 3 + self.opt.load_edge_num
        in_channel = 3 + self.opt.edge_mask_num
        if self.opt.gen_net_type == 'inpaint':
            generator = InpaintGenerator()
            generator.change_in_channel(in_channels=in_channel)
        else:
            generator = cycle_net.define_G(in_channel, 3, 64, self.opt.gen_net_type)

        state_dict = torch.load(load_path, map_location = 'cpu')
        generator.load_state_dict(state_dict, strict = True)
        return generator

    def gen_res_images(self, orgimgs, recimgs):
        if orgimgs.shape[1] != 1:
            orgimgs = orgimgs[:, :1, ...]
            recimgs = recimgs[:, :1, ...]
        self.nonblack_pixels = torch.sum((orgimgs != 0).float(), dim=[-1, -2, -3])  # [bs, 1, h, w]

        if self.opt.cut_thred > 0:
            orgimgs = torch.where(orgimgs > self.opt.cut_thred / 255, orgimgs, torch.zeros(orgimgs.shape).cuda())
            recimgs = torch.where(recimgs > self.opt.cut_thred / 255, recimgs, torch.zeros(recimgs.shape).cuda())

        l_res = recimgs - orgimgs
        l_res = torch.where(l_res >= (self.opt.l_pixel_thred_low / 255), l_res, torch.zeros(l_res.shape).cuda())
        l_res = torch.where(l_res <= (self.opt.l_pixel_thred_high / 255), l_res, torch.zeros(l_res.shape).cuda())

        h_res = orgimgs - recimgs
        h_res = torch.where(h_res >= (self.opt.h_pixel_thred_low / 255), h_res, torch.zeros(h_res.shape).cuda())
        h_res = torch.where(h_res <= (self.opt.h_pixel_thred_high / 255), h_res, torch.zeros(h_res.shape).cuda())

        resimgs = torch.cat([l_res, h_res, abs(recimgs - orgimgs)], dim=1) #[low density, high density, abs]

        return resimgs

    def gen_net_input(self):
        org_img = self.org_image[:, :1, ...]  # [bs,1,h,w]
        rec_img = self.rec_image[:, :1, ...]  # [bs,1,h,w]
        res_img = self.res_image #[bs, 3, h, w]
        self.l_score, self.h_score, self.post_res_image = self.cal_res_scores(org_img, rec_img, self.nonblack_pixels)
        if self.opt.use_post:
            if self.in_channels == 1:
                temp_input = res_img[:, -1:, ...]
            elif self.in_channels == 2:
                temp_input = res_img[:, :-1, ...]  # [bs, 2, h, w]
            elif self.in_channels == 3:
                temp_input = torch.cat([res_img[:, -1:, ...], self.post_res_image], dim=1)  # [bs, 4, h, w]
            elif self.in_channels == 4:
                temp_input = torch.cat([res_img[:, :-1, ...], self.post_res_image], dim=1)  # [bs, 4, h, w]
            else:
                assert False, "Not support input channels = {:.0f} with postprocessing image".format(self.in_channels)
        elif self.opt.use_org:
            if self.in_channels == 1:
                temp_input = org_img
            elif self.in_channels == 2:
                temp_input = torch.cat([org_img, res_img[:, -1:, ...]], dim=1)
            elif self.in_channels == 3:
                temp_input = torch.cat([org_img, res_img[:, :-1, ...]], dim=1)
            else:
                assert False, "Not support input channels = {:.0f} with original image".format(self.in_channels)
        else:
            if self.in_channels == 1:
                temp_input = res_img[:, -1:, ...]
            elif self.in_channels == 2:
                temp_input = res_img[:, :-1, ...]
            elif self.in_channels == 3:
                temp_input = res_img
            else:
                assert False, "Not support input channels = {:.0f} without postprocessing and original ct image".format(self.in_channels)

        self.oc_input = temp_input

        return

    def cal_res_scores(self, orgimgs, recimgs, nonzero_nums):
        l_res = recimgs - orgimgs
        l_res = torch.where(l_res >= (self.opt.l_pixel_thred_low / 255), l_res, torch.zeros(l_res.shape).cuda())
        l_res = torch.where(l_res <= (self.opt.l_pixel_thred_high / 255), l_res, torch.zeros(l_res.shape).cuda())

        l_loss = torch.where(l_res != 0, torch.ones(l_res.shape).cuda() * 255, l_res)
        l_loss = l_loss / 255
        l_bg_weight = F.conv2d(input=l_loss, weight=self.bg_conv_weight, bias=None, stride=1, padding=self.opt.bg_width)

        h_res = orgimgs - recimgs
        h_res = torch.where(h_res >= (self.opt.h_pixel_thred_low / 255), h_res, torch.zeros(h_res.shape).cuda())
        h_res = torch.where(h_res <= (self.opt.h_pixel_thred_high / 255), h_res, torch.zeros(h_res.shape).cuda())

        h_loss = torch.where(h_res != 0, torch.ones(h_res.shape).cuda() * 255, h_res)
        h_loss = h_loss / 255
        h_bg_weight = F.conv2d(input=h_loss, weight=self.bg_conv_weight, bias=None, stride=1, padding=self.opt.bg_width) #[0;1/(2*bg_width+1);1]

        temp = 3
        l_bg_maxv = l_bg_weight
        h_bg_maxv = h_bg_weight
        while temp > 0:
            l_bg_maxv, _ = torch.max(l_bg_maxv, dim=temp, keepdim=True)
            h_bg_maxv, _ = torch.max(h_bg_maxv, dim=temp, keepdim=True)
            temp -= 1
        l_bg_maxv = torch.where(l_bg_maxv == 0, torch.ones_like(l_bg_maxv).cuda(), l_bg_maxv)
        h_bg_maxv = torch.where(h_bg_maxv == 0, torch.ones_like(h_bg_maxv).cuda(), h_bg_maxv)

        l_bg_weight /= l_bg_maxv #[0,1]
        l_bg_weight *= self.opt.l_exp_ratio
        l_bg_weight = torch.exp(l_bg_weight)
        l_bias_img = l_bg_weight * l_loss
        l_bias_img = self.norm255(l_bias_img) / 255

        h_bg_weight /= h_bg_maxv
        h_bg_weight *= self.opt.h_exp_ratio
        h_bg_weight = torch.exp(h_bg_weight)
        h_bias_img = h_bg_weight * h_loss
        h_bias_img = self.norm255(h_bias_img) / 255

        bias_image = torch.cat([l_bias_img, h_bias_img], dim=1)

        l_sum = torch.sum(l_bias_img, dim=[-1, -2, -3])
        h_sum = torch.sum(h_bias_img, dim=[-1, -2, -3])
        l_scores = l_sum / nonzero_nums
        h_scores = h_sum / nonzero_nums

        return l_scores, h_scores, bias_image  # len=bs

    def forward(self):
        gen_outputs = self.gen_images()
        self.org_image = torch.cat([self.or_input[:, 0, :1, ...]] * 3, dim=1)
        self.rec_image = torch.cat([gen_outputs[:, :1, ...]]*3, dim=1)
        self.res_image = self.gen_res_images(self.org_image, self.rec_image)
        self.gen_net_input()

        _, self.output_h = self.net_discriminator(self.oc_input) # [3N, d]
        if not self.opt.no_normalize:
            self.output_h = F.normalize(self.output_h, p=2, dim=1)
        self.score = torch.mean(self.l2_loss(self.output_h, torch.cat([self.center] * self.input_size, dim=0)), dim=-1) # N

        compact_loss = 0  # centralize negative samples
        collapse_loss = 0  # penalty of centralized positive samples
        labels = self.label.tolist()
        neg_ind = [k for k in range(self.input_size) if labels[k] == 0]
        pos_ind = [k for k in range(self.input_size) if labels[k] == 1]

        if len(neg_ind) > 0:
            # neg_center = torch.mean(self.output_h[neg_ind], dim=0, keepdim=True)
            # print('center:', neg_center.shape)
            # print('samples:', self.output_h[neg_ind].shape)
            loss_compact = self.l2_loss(self.output_h[neg_ind], torch.cat([self.center] * len(neg_ind), dim=0)).mean()
            loss_compact = loss_compact / self.temperature
            # print('compact loss = ', loss_compact)
            compact_loss += loss_compact * self.opt.compact_weight

        if len(pos_ind) > 0:
            loss_collapse = self.l2_loss(self.output_h[pos_ind], torch.cat([self.center] * len(pos_ind), dim=0)).mean()
            loss_collapse = 1 / (0.01 + loss_collapse)
            # print('collapse loss = ', loss_collapse)
            collapse_loss += loss_collapse * self.opt.collapse_weight

        self.loss_compact = compact_loss
        self.loss_collapse = collapse_loss
        self.loss_c = compact_loss + collapse_loss

        if self.opt.vis_debug:
            if self.opt.l_state == 'train':
                save_dir = os.path.join(self.vis_dir, 'train')
                mkdir(save_dir)
                org_images = self.org_image[:, :1, ...] * 255
                rec_images = self.rec_image[:, :1, ...] * 255
                res_images = self.res_image * 255
                post_res_images = self.post_res_image * 255
                for i in range(self.input_size):
                    org_img = torch.stack([org_images[i, 0, ...]] * 3, dim=-1).cpu().numpy().astype('uint8')
                    rec_img = torch.stack([rec_images[i, 0, ...]] * 3, dim=-1).cpu().numpy().astype('uint8')
                    res_img = torch.stack(
                        [torch.cat([res_images[i, -1, ...], res_images[i, 0, ...], res_images[i, 1, ...]], dim=-1)] * 3,
                        dim=-1).cpu().numpy().astype('uint8')
                    post_res_img = torch.stack(
                        [torch.cat([post_res_images[i, 0, ...], post_res_images[i, 1, ...]], dim=-1)] * 3,
                        dim=-1).cpu().numpy().astype('uint8')
                    save_image = np.concatenate([org_img, rec_img, res_img, post_res_img], axis=1)
                    save_path = os.path.join(save_dir, self.scan_list[i] + '_' + str(self.slice_id[i]) + '_' + str(self.label[i].item()) + '.jpg')
                    if not os.path.exists(save_path):
                        cv2.imwrite(save_path, save_image)

    def stat_info(self):
        self.label = self.label.cpu().long().tolist()
        self.score = self.score.detach().cpu().tolist()
        # self.slice_id = self.slice_id.tolist()
        self.l_score = self.l_score.detach().cpu().tolist()
        self.h_score = self.h_score.detach().cpu().tolist()
        # print(len(self.label))
        # print(len(self.score))
        # print('----------')
        self.buffer_glabels.extend(self.label)
        self.buffer_gscores.extend(self.score)
        self.buffer_glscores.extend(self.l_score)
        self.buffer_ghscores.extend(self.h_score)
        self.buffer_gscan_ids.extend(self.scan_list)
        self.buffer_ginput_ids.extend(self.input_id)

        if self.opt.l_state != 'train' and self.opt.vis_cluster:
            self.feature = self.output_h.detach() #[N,d]
            self.buffer_gfeatures.extend(self.feature)

        if self.opt.l_state != 'train' and (self.opt.vis_slice or self.opt.vis_scan):
            org_images = self.norm255(self.org_image)  # [bs,3,h,w]
            rec_images = self.norm255(self.rec_image)  # [bs,3,h,w]
            res_images = self.res_image * 255  # [bs,3,h,w]
            # post_images = self.post_res_image * 255 # [bs,2,h,w]
            post_images = self.post_res_image  # [bs,2,h,w]

            if self.opt.vis_slice:
                save_dir = os.path.join(self.vis_dir, 'slice')
                mkdir(save_dir)
            for i in range(self.input_size):
                org_img = torch.stack([org_images[i, 0, ...]] * 3, dim=-1).cpu().numpy().astype('uint8')
                rec_img = torch.stack([rec_images[i, 0, ...]] * 3, dim=-1).cpu().numpy().astype('uint8')
                res_img_l_post = torch.stack([self.norm255(post_images[i, 0, ...])] * 3, dim=-1).cpu().numpy()
                res_img_h_post = torch.stack([self.norm255(post_images[i, 1, ...])] * 3, dim=-1).cpu().numpy()
                res_mask = res_img_l_post + res_img_h_post
                res_mask = np.where(res_mask > 255, 255, res_mask).astype('uint8')
                # res_mask = self.norm255(res_mask).astype('uint8')
                post_compose = res_mask
                # post_compose = torch.stack([self.norm255(res_images[i, -1, ...])] * 3, dim=-1).cpu().numpy().astype('uint8')
                heatmap = cv2.addWeighted(org_img, 0.4, cv2.applyColorMap(post_compose, cv2.COLORMAP_JET), 0.6, 0)

                if self.opt.vis_slice:
                    slice_image = np.concatenate([org_img, rec_img, heatmap], axis=1)
                    save_path = os.path.join(save_dir, str(self.label[i]) + '_' + self.scan_list[i] + '_' + str(
                        self.slice_id[i]) + '.jpg')
                    if not os.path.exists(save_path):
                        cv2.imwrite(save_path, slice_image)
                if self.opt.vis_scan:
                    scanid = self.scan_list[i]
                    if scanid not in self.scan_dict.keys():
                        self.scan_dict[scanid] = {}
                        self.scan_dict[scanid]['label'] = 0
                        self.scan_dict[scanid]['slice_label'] = []
                        self.scan_dict[scanid]['slice_score'] = []
                        self.scan_dict[scanid]['org_image'] = []
                        self.scan_dict[scanid]['rec_image'] = []
                        self.scan_dict[scanid]['res_mask'] = []
                        self.scan_dict[scanid]['heatmap'] = []
                        self.scan_dict[scanid]['slice_id'] = []

                    self.scan_dict[scanid]['label'] = max(self.scan_dict[scanid]['label'], self.label[i])
                    self.scan_dict[scanid]['slice_label'].append(self.label[i])
                    self.scan_dict[scanid]['slice_score'].append(self.score[i])
                    self.scan_dict[scanid]['org_image'].append(org_img)
                    self.scan_dict[scanid]['rec_image'].append(rec_img)
                    self.scan_dict[scanid]['res_mask'].append(res_mask)
                    self.scan_dict[scanid]['heatmap'].append(heatmap)
                    self.scan_dict[scanid]['slice_id'].append(self.slice_id[i])

    def backward(self):
        self.update_metrics('local')
        loss = 0
        if not len(self.loss_names):
            return

        for name in self.loss_names:
            if name == 'c':
                loss += getattr(self,'loss_' + name) / self.opt.grad_iter_size
                break
        if isinstance(loss, float):
            print('loss is a float scalar, no backward conducting.')
            return
        loss.backward()

    def validation(self, dataset, visualizer, valid_iter):
        self.eval()
        o_l_state = self.opt.l_state
        self.opt.l_state = 'valid'

        iter_time_meter = metrics.TimeMeter()
        data_time_meter = metrics.TimeMeter()

        data_time_meter.start()
        iter_time_meter.start()

        for i, data in enumerate(dataset):  # inner loop within one epoch
            data_time_meter.record(n = self.opt.batch_size)
            iter_time_meter.start()
            self.set_input(data)
            self.validate_parameters()
            self.update_metrics('local')

            iter_time_meter.record()

            if i % self.opt.v_print_freq == 0:  # print training losses and save logging information to the disk
                visualizer.print_current_info(-1, i, self, iter_time_meter.val, data_time_meter.val)
                self.save_stat_info(local=True)

            data_time_meter.start()
            iter_time_meter.start()

        if self.opt.multi_vsets > 1:
            combine_with_id(self)

        self.assign_scan()
        self.update_metrics('global')
        visualizer.print_global_info(-1, -1, self, iter_time_meter.sum/60, data_time_meter.sum/60)
        visualizer.plot_global_info(self, valid_iter, ptype='valid')
        self.train()
        self.save_stat_info()
        self.global_visualize()
        self.opt.l_state = o_l_state
        # self.reset_meters()
        self.clear_info()

    def assign_scan(self):
        tmp_dict = {}
        # print(len(self.buffer_glabels))
        # print(len(self.buffer_gscores))
        for sid, spath, label, score, l_score, h_score in zip(self.buffer_gscan_ids, self.buffer_ginput_ids,
                                                              self.buffer_glabels, self.buffer_gscores,
                                                              self.buffer_glscores, self.buffer_ghscores):
            if l_score < 0 or h_score < 0:
                continue
            # sid = spath.split('/')[-2]
            if sid not in tmp_dict.keys():
                tmp_dict[sid] = {}
                tmp_dict[sid]['label'] = 0
                tmp_dict[sid]['scores'] = []
                tmp_dict[sid]['l_scores'] = []
                tmp_dict[sid]['h_scores'] = []
                tmp_dict[sid]['paths'] = []
            tmp_dict[sid]['label'] = max(tmp_dict[sid]['label'], label)
            tmp_dict[sid]['scores'].append(score)
            tmp_dict[sid]['l_scores'].append(l_score)
            tmp_dict[sid]['h_scores'].append(h_score)
            tmp_dict[sid]['paths'].append(spath)

        for k, item in tmp_dict.items():
            self.buffer_scanlabels.append(item['label'])
            self.buffer_scanpaths.append(item['paths'][0])
            self.buffer_scanscores.append(np.mean(item['scores']))
            self.buffer_scanlscores.append(np.mean(item['l_scores']))
            self.buffer_scanhscores.append(np.mean(item['h_scores']))

        self.buffer_gscores = np.array(self.buffer_gscores)
        self.buffer_glscores = np.array(self.buffer_glscores)
        self.buffer_ghscores = np.array(self.buffer_ghscores)
        self.buffer_slicepscores = (self.buffer_glscores + self.buffer_ghscores) / 2

        self.buffer_scanscores = np.array(self.buffer_scanscores)
        self.buffer_scanlscores = np.array(self.buffer_scanlscores)
        self.buffer_scanhscores = np.array(self.buffer_scanhscores)
        self.buffer_scanpscores = (self.buffer_scanlscores + self.buffer_scanhscores) / 2


        if self.opt.l_state != 'train' and self.opt.vis_cluster:
            tmp_f_dict = {}
            for sid, spath, feature in zip(self.buffer_gscan_ids, self.buffer_ginput_ids, self.buffer_gfeatures):
                if sid not in tmp_f_dict.keys():
                    tmp_f_dict[sid] = []
                tmp_f_dict[sid].append(feature)

            for k, item in tmp_f_dict.items():
                scanf = torch.mean(torch.stack(item, dim=0), dim=0, keepdim=True)  # [1,d]
                self.buffer_scanfeatures.append(scanf)  # [[1,d],[1,d], ...]

    def visualize(self):
        pass

    def global_visualize(self):
        if self.opt.vis_cluster:
            save_dir = os.path.join(self.save_dir, 'features')
            mkdir(save_dir)

            """save slice feature array"""
            slice_features = torch.stack(self.buffer_gfeatures, dim=0).cpu().numpy()  # [slice num, d]
            np.save(os.path.join(save_dir, 'slice_features.npy'), slice_features)

            slice_labels = np.array(self.buffer_glabels)
            np.save(os.path.join(save_dir, 'slice_labels.npy'), slice_labels)

            """save scan feature array"""
            scan_features = torch.cat(self.buffer_scanfeatures, dim=0)  # [scan_num, d]
            scan_features = scan_features.cpu().numpy()
            np.save(os.path.join(save_dir, 'scan_features.npy'), scan_features)

            scan_labels = np.array(self.buffer_scanlabels)
            np.save(os.path.join(save_dir, 'scan_labels.npy'), scan_labels)

        if self.opt.l_state != 'train' and self.opt.save_pred_score:
            f = open(osp.join(self.save_dir, 'pred_result_scan.txt'), 'w')
            for i in range(len(self.buffer_scanscores)):
                label = self.buffer_scanlabels[i]
                score = self.buffer_scanscores[i]
                ids = self.buffer_scanpaths[i]
                f.write(str(i) + ' ' + str(score) + ' ' + str(label) + ' ' + str(ids) + '\n')
            f.close()

            f = open(osp.join(self.save_dir, 'pred_result_slice.txt'), 'w')
            for i in range(len(self.buffer_gscores)):
                label = self.buffer_glabels[i]
                score = self.buffer_gscores[i]
                ids = self.buffer_ginput_ids[i]
                f.write(str(i) + ' ' + str(score) + ' ' + str(label) + ' ' + str(ids) + '\n')
            f.close()

            f = open(osp.join(self.save_dir, 'pred_result_value.txt'), 'w')
            for i in range(len(self.buffer_scanpscores)):
                label = self.buffer_scanlabels[i]
                score = self.buffer_scanpscores[i]
                ids = self.buffer_scanpaths[i]
                f.write(str(i) + ' ' + str(score) + ' ' + str(label) + ' ' + str(ids) + '\n')
            f.close()

        if self.opt.l_state != 'train' and self.opt.vis_scan:
            save_dir = os.path.join(self.vis_dir, 'scan')
            mkdir(save_dir)
            max_scan_score = max([np.mean(item['slice_score']) for item in self.scan_dict.values()])
            for sid in self.scan_dict.keys():

                scan_label = self.scan_dict[sid]['label']
                scan_image = []

                inds = np.argsort(self.scan_dict[sid]['slice_id'])
                for key, value in self.scan_dict[sid].items():
                    if key == 'label':
                        continue
                    self.scan_dict[sid][key] = [value[k] for k in inds]

                for p in range(len(self.scan_dict[sid]['org_image'])):
                    org_img = self.scan_dict[sid]['org_image'][p]
                    cv2.putText(org_img, str(self.scan_dict[sid]['slice_id'][p]), (25, 25), cv2.FONT_HERSHEY_SIMPLEX,
                                0.75, (0, 0, 255), 2)
                    temp_img = np.concatenate(
                        [org_img, self.scan_dict[sid]['rec_image'][p], self.scan_dict[sid]['heatmap'][p]], axis=0)
                    scan_image.append(temp_img)

                scan_score = '{:.4f}'.format(np.mean(self.scan_dict[sid]['slice_score']) / max_scan_score)
                scan_image = np.stack(scan_image, axis=0)  # NHWC
                concat_image = cat_image(scan_image, h_num=-1, in_type='HWC', out_type='HWC', factor=1)
                save_path = os.path.join(save_dir, str(scan_score)+ '_' + str(scan_label) + '_' + str(sid) + '.jpg')
                if not os.path.exists(save_path):
                    cv2.imwrite(save_path, concat_image)

    def clear_info(self):
        for name in self.buffer_names:
            if name == 'names':
                continue

            tmp_buffer = []
            setattr(self,'buffer_' + name,tmp_buffer)


    def cal_loss(self):
        pass

    def cal_g_metric(self):
        if self.opt.l_state == 'train':
            if 'auc' in str(self.g_metric_names):
                self.g_metric_auc = metrics.auc_score(self.buffer_glabels, self.buffer_gscores)
                self.g_metric_auc_pscore = metrics.auc_score(self.buffer_glabels, self.buffer_slicepscores)
                self.g_metric_auc_slice = metrics.auc_score(self.buffer_glabels, self.buffer_gscores)
            if 'ap' in str(self.g_metric_names):
                self.g_metric_ap = metrics.ap_score(self.buffer_glabels, self.buffer_gscores)
            if 'sensitivity' in str(self.g_metric_names) or 'specificity' in str(self.g_metric_names):
                self.g_metric_sensitivity, self.g_metric_specificity = metrics.roc(self.buffer_glabels, self.buffer_gscores)
            if 'precision' in self.g_metric_names:
                self.g_metric_precision = metrics.precision(self.t_metric_cmatrix, 1)
            if 'recall' in self.g_metric_names:
                self.g_metric_recall = metrics.recall(self.t_metric_cmatrix, 1)
            return

        if 'auc' in str(self.g_metric_names):
            self.g_metric_auc = metrics.auc_score(self.buffer_scanlabels, self.buffer_scanscores)
            self.g_metric_auc_pscore = metrics.auc_score(self.buffer_scanlabels, self.buffer_scanpscores)
            self.g_metric_auc_slice = metrics.auc_score(self.buffer_glabels, self.buffer_gscores)
        if 'ap' in str(self.g_metric_names):
            self.g_metric_ap = metrics.ap_score(self.buffer_scanlabels, self.buffer_scanscores)
        if 'sensitivity' in str(self.g_metric_names) or 'specificity' in str(self.g_metric_names):
            self.g_metric_sensitivity, self.g_metric_specificity = metrics.roc(self.buffer_scanlabels, self.buffer_scanscores)
        if 'precision' in self.g_metric_names:
            self.g_metric_precision = metrics.precision(self.t_metric_cmatrix, 1)
        if 'recall' in self.g_metric_names:
            self.g_metric_recall = metrics.recall(self.t_metric_cmatrix, 1)

    def cal_t_metric(self):
        if self.opt.l_state == 'train':
            if 'cmatrix' in self.t_metric_names:
                optimal_thred = metrics.best_thred(self.buffer_glabels, self.buffer_gscores)
                temp_gscores = np.squeeze(self.buffer_gscores)
                self.buffer_slicepreds = np.where(temp_gscores >= optimal_thred, np.ones_like(temp_gscores), np.zeros_like(temp_gscores))
                # print(self.buffer_slicepreds)
                # exit()
                self.t_metric_cmatrix = metrics.comfusion_matrix(self.buffer_slicepreds, self.buffer_glabels,
                                                                 self.opt.class_num)
            return

        if 'cmatrix' in self.t_metric_names:
            optimal_thred = metrics.best_thred(self.buffer_scanlabels, self.buffer_scanscores)
            self.buffer_scanpreds = np.where(self.buffer_scanscores >= optimal_thred,
                                             np.ones_like(self.buffer_scanscores),
                                             np.zeros_like(self.buffer_scanscores)).tolist()
            self.t_metric_cmatrix = metrics.comfusion_matrix(self.buffer_scanpreds, self.buffer_scanlabels,
                                                             self.opt.class_num)

    def postprocess(self, img):
        img = img * 255.0
        img = img.permute(0, 2, 3, 1)
        return img.int()

    def norm255(self, img):
        minv = img
        maxv = img
        tmp_num = len(minv.shape)
        while tmp_num > 1:
            if isinstance(img, np.ndarray):
                minv = np.min(minv, axis=tmp_num - 1, keepdims=True)
                maxv = np.max(maxv, axis=tmp_num - 1, keepdims=True)
                tmp_num -= 1
            else:
                minv, _ = torch.min(minv, dim=tmp_num - 1, keepdim=True)
                maxv, _ = torch.max(maxv, dim=tmp_num - 1, keepdim=True)
                tmp_num -= 1

        p = maxv - minv
        if isinstance(img, np.ndarray):
            p = np.where(p == 0, np.ones_like(p), p)
        else:
            p = torch.where(p == 0, torch.ones_like(p), p)

        img = (img - minv) * (255 / p)
        return img

    def load_networks(self, epoch, load_dir ='', strict = True):
        """Load all the networks from the disk.

        Parameters:
            epoch (int) -- current epoch; used in the file name '%s_net_%s.pth' % (epoch, name)
        """
        for name in self.net_names:
            if isinstance(name, str):
                load_filename = '%s_net_%s.pth' % (epoch, name)
                if load_dir == '':
                    load_dir = self.save_dir

                load_path = os.path.join(load_dir, load_filename)
                print('load path', load_path)

                if not osp.exists(load_path):
                    print('net ' + name + ' has no state dict')
                    continue

                net = getattr(self, 'net_' + name)
                if isinstance(net, torch.nn.DataParallel):
                    net = net.module
                print('loading the model from %s' % load_path)
                state_dict = torch.load(load_path, map_location = 'cpu')
                net.load_state_dict(state_dict, strict = strict)

