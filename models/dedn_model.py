import torch
import torch.nn as nn
import torch.optim as optim
from .base_model import BaseModel
import torch.nn.functional as F
from .networks import cycle_net
from .networks.inpaint_net import InpaintGenerator, Discriminator
from util.loss.inpaint_loss import AdversarialLoss, PerceptualLoss, StyleLoss
from util.basic import *
from util import metrics
from util.metrics.psnr import PSNR
from util.image import save_debug_image

class DeDNModel(BaseModel):
    @staticmethod
    def modify_commandline_options(parser):
        parser.add_argument('--EDGE_THRESHOLD', type=float, default=0.5)
        parser.add_argument('--L1_LOSS_WEIGHT', type=float, default=1)
        parser.add_argument('--FM_LOSS_WEIGHT', type=float, default=10)
        parser.add_argument('--STYLE_LOSS_WEIGHT', type=float, default=10)
        parser.add_argument('--CONTENT_LOSS_WEIGHT', type=float, default=0.1)
        parser.add_argument('--INPAINT_ADV_LOSS_WEIGHT', type=float, default=0.1)
        parser.add_argument('--GAN_LOSS', type=str, default='nsgan')
        parser.add_argument('--GAN_POOL_SIZE', type=int, default=0)
        parser.add_argument('--save_merged_image', type=int, default=1)
        parser.add_argument('--edge_mask_num', type=int, default=1)
        parser.add_argument('--multi_edge', type=int, default=1)
        parser.add_argument('--gen_net_type', type=str, default='inpaint')
        parser.add_argument('--load_edge_num', type=int, default=1)
        return parser

    def __init__(self, opt):
        super().__init__(opt)

        self.buffer_ginput_ids = []
        self.buffer_gscores = []
        self.buffer_glabels = []
        self.buffer_scanscores = []
        self.buffer_scanlabels = []
        self.buffer_scanids = []
        '''The parameters to be reset for a new model'''

        self.loss_names = ['dis','gen'] # used to update networks,
        self.s_metric_names = ['mae', 'psnr','l1','content','style'] # scalar metric, stat local infomation
        self.g_metric_names = ['auc'] # scalar metric, stat global infomation
        self.t_metric_names = [] # table or matrix metric
        self.net_names = ['generator','discriminator']

        self.valid_metric = 'l1'
        self.scheduler_metric = 'l1'
        self.valid_metric_dir = -1
        self.scheduler_metric_dir = -1

        print('net type', self.opt.gen_net_type)
        if self.opt.gen_net_type == 'inpaint':
            self.net_generator = InpaintGenerator()
            self.net_generator.change_in_channel(in_channels=3 + self.opt.edge_mask_num)
        else:
            self.net_generator = cycle_net.define_G(3 + self.opt.edge_mask_num, 3, 64, self.opt.gen_net_type)

        self.net_discriminator = Discriminator(in_channels=3, use_sigmoid=opt.GAN_LOSS != 'hinge')

        self.l1_loss = nn.L1Loss().cuda()
        self.perceptual_loss = PerceptualLoss().cuda()
        self.style_loss = StyleLoss().cuda()
        self.adversarial_loss = AdversarialLoss(type=opt.GAN_LOSS).cuda()

        self.psnr = PSNR(255.0).cuda()

        self.exp_ratio = 1
        self.pixel_thred = 20
        self.bg_width = 3
        self.bg_conv_weight = torch.ones(1,1, self.bg_width * 2 + 1, self.bg_width * 2 + 1).cuda()

    @staticmethod
    def supply_option_info(opt):
        if opt.l_state == 'train':
            InpaintModel.set_train_load_dir(opt)
        else:
            InpaintModel.set_valid_load_dir(opt)
        return opt

    def set_input(self, data):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.

        Parameters:
            input (dict): includes the data itself and its metadata information.
        """

        self.input, _, self.edges, self.masks, self.label, self.input_id = data
        self.input = self.input.cuda()
        self.edges = self.edges.cuda()
        self.masks = self.masks.cuda()
        self.input_size = self.input.shape[0]

        if self.opt.l_state != 'train':
            assert self.input.shape[0] == 1
            self.input = self.input[0]
            self.edges = self.edges[0]
            self.masks = self.masks[0]

    def change_net_structure(self):
        if self.opt.l_state == 'train' and self.opt.gen_net_type == 'inpaint':
            self.net_generator.change_in_channel(in_channels=3 + self.opt.edge_mask_num)

    def set_optimizer(self,opt):

        generator = self.net_generator
        discriminator = self.net_discriminator


        if opt.op_name == 'SGD':
            print('op name', opt.op_name)
            self.gen_optimizer = optim.SGD(generator.parameters(), lr=opt.lr, momentum = opt.momentum, nesterov=opt.nesterov,
                                        weight_decay = opt.weight_decay)

            self.dis_optimizer = optim.SGD(discriminator.parameters(), lr=opt.lr * opt.d2g_lr, momentum = opt.momentum, nesterov=opt.nesterov,
                                           weight_decay = opt.weight_decay)

        else:
            print('op name', opt.op_name)
            self.gen_optimizer = optim.Adam(
                params=generator.parameters(),
                lr=float(opt.lr),
                betas=(opt.beta1, opt.beta2)
            )

            self.dis_optimizer = optim.Adam(
                params=discriminator.parameters(),
                lr=float(opt.lr) * float(opt.d2g_lr),
                betas=(opt.beta1, opt.beta2)
            )

        self.optimizers = [self.gen_optimizer, self.dis_optimizer]


    def forward(self):
        images = self.input
        edges = self.edges
        masks = self.masks
        images_masked = (images * (1 - masks).float()) + masks
        inputs = torch.cat((images_masked, edges), dim=1)
        outputs = self.net_generator(inputs)                                    # in: [rgb(3) + edge(1)]
        self.outputs = outputs

        gen_loss = 0
        dis_loss = 0

        # discriminator loss
        dis_input_real = images
        dis_input_fake = outputs.detach()
        dis_real, _ = self.net_discriminator(dis_input_real)                    # in: [rgb(3)]
        dis_fake, _ = self.net_discriminator(dis_input_fake)                    # in: [rgb(3)]
        dis_real_loss = self.adversarial_loss(dis_real, True, True)
        dis_fake_loss = self.adversarial_loss(dis_fake, False, True)
        dis_loss += (dis_real_loss + dis_fake_loss) / 2


        # generator adversarial loss
        gen_input_fake = outputs
        gen_fake, _ = self.net_discriminator(gen_input_fake)                    # in: [rgb(3)]
        gen_gan_loss = self.adversarial_loss(gen_fake, True, False) * self.opt.INPAINT_ADV_LOSS_WEIGHT
        gen_loss += gen_gan_loss


        # generator l1 loss
        gen_l1_loss = self.l1_loss(outputs, images) / torch.mean(masks)
        self.s_metric_l1 = gen_l1_loss
        gen_loss += gen_l1_loss * self.opt.L1_LOSS_WEIGHT

        # generator perceptual loss
        gen_content_loss = self.perceptual_loss(outputs, images)
        self.s_metric_content = gen_content_loss
        gen_loss += gen_content_loss * self.opt.CONTENT_LOSS_WEIGHT


        # generator style loss
        gen_style_loss = self.style_loss(outputs * masks, images * masks)
        self.s_metric_style = gen_style_loss
        gen_loss += gen_style_loss * self.opt.STYLE_LOSS_WEIGHT

        self.loss_dis = dis_loss
        self.loss_gen = gen_loss


        if self.opt.l_state == 'train':
            outputs_merged = (outputs * masks) + (images * (1 - masks))
            self.outputs_merged = outputs_merged
        else:
            outputs_merged = (outputs * masks).sum(dim =0, keepdim = True)
            self.outputs_merged = outputs_merged


    def validate_parameters(self):
        if not self.start_forward:
            return
        if self.opt.vis_method != 'gradcam':
            with torch.no_grad():
                self.forward()
        else:
            self.forward()
        self.stat_info()

    def optimize_parameters(self):
        """Update network weights; it will be called in every training iteration."""
        if not self.start_forward:
            return
        self.forward()
        self.backward()
        self.stat_info()
        self.c_grad_iter += 1

        if self.c_grad_iter == self.opt.grad_iter_size:
            self.clip_grad()
            for optimizer in self.optimizers:
                optimizer.step()
                optimizer.zero_grad()
            self.c_grad_iter = 0

    def stat_info(self):
        if self.opt.l_state != 'train':


            org_img = self.norm255(self.input[:1,:1,...])
            rec_img = self.norm255(self.outputs_merged[:,:1,...])


            loss = torch.abs(org_img - rec_img)
            loss = torch.where(loss >= self.pixel_thred, loss, torch.zeros(loss.shape).cuda())
            black_tensor = (org_img == 0).int().sum(dim = [-1,-2,-3]).cuda()

            loss = loss / 255
            bg_weight = F.conv2d(input = loss, weight =self.bg_conv_weight, bias = None, stride = 1, padding = self.bg_width)

            loss = loss * self.exp_ratio
            loss =torch.exp(bg_weight) * loss

            total_num = loss.shape[-1] * loss.shape[-2] * loss.shape[-3]
            total_tensor = torch.Tensor([total_num] * loss.shape[0]).cuda()

            self.score = loss.sum(dim = [-1,-2,-3]) / (total_tensor - black_tensor)

        self.score = self.score.detach().cpu().tolist()
        self.label = self.label.cpu().tolist()
        self.buffer_gscores.extend(self.score)
        self.buffer_glabels.extend(self.label)
        self.buffer_ginput_ids.extend(self.input_id)

    def visualize(self):
        pass

    def cal_loss(self):
        pass

    def cal_s_metric(self):
        images = self.input

        if self.opt.l_state == 'train':
            psnr = self.psnr(self.postprocess(images), self.postprocess(self.outputs_merged))
            mae = (torch.sum(torch.abs(images - self.outputs_merged)) / torch.sum(images)).float()
        else:
            psnr = self.psnr(self.postprocess(images[:1]), self.postprocess(self.outputs_merged))
            mae = (torch.sum(torch.abs(images[:1] - self.outputs_merged)) / torch.sum(images[:1])).float()
            # print('mae', mae)

        self.s_metric_psnr = psnr
        self.s_metric_mae = mae

        if self.opt.l_state == 'train':
            self.score = torch.Tensor([-1] * 6)

    def cal_g_metric(self):
        if self.opt.l_state == 'train':
            self.g_metric_auc = -1
            return

        tmp_dict = {}
        for spath, label, score in zip(self.buffer_ginput_ids, self.buffer_glabels, self.buffer_gscores):
            sid = spath.split('/')[-2]
            if sid not in tmp_dict.keys():
                tmp_dict[sid] = {}
                tmp_dict[sid]['label'] = 0
                tmp_dict[sid]['score'] = []

            tmp_dict[sid]['label'] = max(0, label)
            tmp_dict[sid]['score'].append(score)

        for k, item in tmp_dict.items():
            self.buffer_scanlabels.append(item['label'])
            self.buffer_scanscores.append(np.mean(item['score']))
            self.buffer_scanids.append(k)

        self.buffer_scanscores = np.array(self.buffer_scanscores)
        # print('score shape', self.buffer_scanscores.shape)
        self.buffer_scanscores = self.buffer_scanscores / np.max(self.buffer_scanscores).item()
        self.g_metric_auc = metrics.auc_score(self.buffer_scanlabels, self.buffer_scanscores)

    def next_epoch(self):
        pass

    @staticmethod
    def set_train_load_dir(opt):
        if opt.load_dir != '':
            return

        if opt.load_dir_ind < 0:
            return

        load_dict = {
        }
        opt.load_dir = load_dict[opt.load_dir_ind]
        pass

    @staticmethod
    def set_valid_load_dir(opt):
        if opt.load_dir_ind < 0:
            return

        load_dict = {
        }
        opt.load_dir = load_dict[opt.load_dir_ind]

    def postprocess(self, img):
        img = img * 255.0
        img = img.permute(0, 2, 3, 1)
        return img.int()

    def norm255(self,img):
        minv = img
        maxv = img
        tmp_num = len(minv.shape)
        while tmp_num > 1:
            minv, _ = torch.min(minv, dim = -1, keepdim = True)
            maxv, _ = torch.max(maxv, dim = -1, keepdim = True)
            tmp_num -= 1

        img = (img - minv) * (255 / (maxv - minv))
        return img

