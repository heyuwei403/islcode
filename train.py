from util.basic import *
from util.manager import save_manager_info
from util.metrics import TimeMeter
from models import create_model
from options.my_options import TrainOptions
from datasets import create_dataset
from util.visualizer import Visualizer
import os
import torch
os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'

if __name__ == '__main__':
    opt = TrainOptions().parse()   # get training options
    dataset = create_dataset(opt,'train')  # create a dataset given opt.dataset_mode and other options
    dataset_size = len(dataset)    # get the number of images in the dataset.
    print('The number of training images = %d' % dataset_size)
    print('---------------')

    if opt.valid_model:
        v_dataset = create_dataset(opt,'valid')
        print('The number of validation images = %d' % len(v_dataset))
        print('---------------')

    model = create_model(opt)      # create a model given opt.model and other options
    model.setup(opt)               # regular setup: load and print networks; create schedulers
    # if opt.update_center:
    #     model.cal_center(dataset, epoch='init')
    visualizer = Visualizer(opt)   # create a visualizer that display/save images and plots
    opt.total_iters = 0                # the total number of training iterations
    opt.valid_iters = 0

    iter_time_meter = TimeMeter()
    data_time_meter = TimeMeter()
    epoch_time_meter = TimeMeter()

    if hasattr(model, 'set_center'):
        model.set_center(opt, dataset)

    print('Start to train')


    for epoch in range(opt.epoch_count, opt.niter + opt.niter_decay + 1):    # outer loop for different epochs; we save the model by <epoch_count>, <epoch_count>+<save_latest_freq>
        opt.epoch = epoch
        opt.epoch_iter = 0                  # the number of training iterations in current epoch, reset to 0 every epoch
        epoch_time_meter.start()  # timer for entire epoch
        data_time_meter.start()
        iter_time_meter.start()
        for i, data in enumerate(dataset):  # inner loop within one epoch, about 4s to 5s for a batch(batch size = 8)
            data_time_meter.record()
            iter_time_meter.start()
            visualizer.reset()
            opt.total_iters += 1
            opt.epoch_iter += 1
            model.set_input(data)         # unpack data from dataset and apply preprocessing
            model.optimize_parameters()   # calculate loss functions, get gradients, update network weights
            iter_time_meter.record()

            visualizer.plot_current_info(model, opt.total_iters, ptype='train')

            if opt.total_iters % opt.print_freq == 0:    # print training losses and save logging information to the disk
                visualizer.print_current_info(epoch, opt.epoch_iter, model, iter_time_meter.val, data_time_meter.val)

            if opt.total_iters % opt.valid_freq == 0:   # cache our latest model every <save_latest_freq> iterations
                model.assign_scan()
                model.update_metrics('global')
                model.save_networks('train', visualizer)
                visualizer.print_global_info(epoch, opt.epoch_iter, model, iter_time_meter.sum/60,data_time_meter.sum/60)
                visualizer.plot_global_info(model, opt.valid_iters, ptype='train')

                iter_time_meter.reset()
                data_time_meter.reset()

                model.reset_meters()
                model.clear_info()
                if opt.valid_model:
                    model.validation(v_dataset, visualizer, valid_iter= opt.valid_iters)
                model.update_learning_rate()

                save_suffix = 'optimal'
                model.save_networks(save_suffix,visualizer)

                if opt.early_stop and model.wait_over:
                    break

                model.reset_meters()
                opt.valid_iters += 1

            data_time_meter.start()
            iter_time_meter.start()

        epoch_time_meter.record()
        epoch_time_meter.start()
        print('End of epoch %d / %d \t Time Taken: %d hours' % (epoch, opt.niter + opt.niter_decay, epoch_time_meter.sum / 3600.))

        if opt.early_stop and model.wait_over:
            print('early stop at %d / %d' % (epoch,opt.epoch_iter))
            break

        dataset.dataset.next_epoch()
        model.next_epoch()


    '''save the best result'''
    save_manager_info(opt, model.best_m_value)