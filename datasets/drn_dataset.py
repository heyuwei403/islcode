import copy
import cv2
from util.basic import *
from util.video import *
import os
import torch
import torchvision.transforms as transforms
import torchvision.transforms.functional as F
from .base_dataset import BaseDataset
import numpy as np


class DRNDataset(BaseDataset):
    @staticmethod
    def modify_commandline_options(parser):
        parser.add_argument('--dataset_name', type=str, default='retrospective')
        parser.add_argument('--target_focus', type=int, default=1)
        parser.add_argument('--target_focus_name', type=str, default='')
        parser.add_argument('--test_with_train_normal', type=int, default=0)
        parser.add_argument('--window_type', type=str, default='mid2high')
        parser.add_argument('--minimum_slices', type=int, default=6)
        parser.add_argument('--maximum_slices', type=int, default=40)
        parser.add_argument('--drop_slice_num', type=int, default=2)
        parser.add_argument('--resize_dcm_len', type=int, default=30)
        parser.add_argument('--cut_hu', type=int, default=200)
        parser.add_argument('--multi_ww', type=int, default=0)
        parser.add_argument('--norm_data', type=int, default=0)
        parser.add_argument('--sigma', type=float, default=1)
        parser.add_argument('--mask_num', type=int, default=5)
        parser.add_argument('--load_size', type=int, default=256)
        parser.add_argument('--only_target_slice', type=int, default=0)
        parser.add_argument('--with_lunkuo', type=int, default=0)
        parser.add_argument('--with_naoshi_info', type=int, default=0)
        parser.add_argument('--lunkuo_num', type=int, default=0)
        parser.add_argument('--max_mask_num', type=int, default=50)

        parser.add_argument('--train_num', type=int, default=1000)
        parser.add_argument('--target_num', type=int, default=100)
        parser.add_argument('--canny_type', type=int, default=0)
        parser.add_argument('--ww_type', type=str, default='blood')
        parser.add_argument('--canny_thred_1', type=int, default=60)
        parser.add_argument('--canny_thred_2', type=int, default=150)
        parser.add_argument('--neg_ratio', type=float, default=1)
        parser.add_argument('--filter_imaging', type=int, default=1)
        parser.add_argument('--filter_normal', type=int, default=0)
        parser.add_argument('--filter_machine', type=int, default=0)
        parser.add_argument('--filter_age', type=int, default=1)
        parser.add_argument('--filter_black', type=int, default=1)
        parser.add_argument('--black_thred', type=float, default=0.9)
        # test mode
        # if mode == 2:
        parser.add_argument('--input', type=str, help='path to the input images directory or an input image')
        parser.add_argument('--mask', type=str, help='path to the masks directory or a mask file')
        parser.add_argument('--edge', type=str, help='path to the edges directory or an edge file')
        parser.add_argument('--output', type=str, help='path to the output directory')

        parser.add_argument('--test_result', type=int, default=0)
        return parser

    def __init__(self, opt, data_type):
        BaseDataset.__init__(self, opt, data_type)

        self.data_type = data_type
        self.drop_slice_num = opt.drop_slice_num
        self.minimum_slices = opt.minimum_slices
        self.maximum_slices = opt.maximum_slices
        self.use_buffer = False
        self.data_norm_type = 'gray'
        self.image_names = ['input', 'previous', 'later']

        self.img_resize = BaseDataset.get_aug_transform(opt, preprocess='resize' , in_type='HW', out_type='HW')
        self.mask_num = self.opt.mask_num
        self.masks = self.prepare_masks()
        self.unclean_ids = []
        self.canny_list = [(20, 100), (80, 140), (220, 240)]

        self.root_dir = 'data/oct_paths'
        if self.data_type == 'train':
            self.json_path = os.path.join(self.root_dir, 'train_normal.json')
            tmp_normal_data = json.load(open(self.json_path))
            random.shuffle(tmp_normal_data)
            
            if self.opt.train_num > 0:
                tmp_normal_data = tmp_normal_data[:self.opt.train_num]

            print('abnormal scan num: 0')
            print('normal scan num:', len(tmp_normal_data))

            real_normal_path_list, real_normal_label_list, real_normal_id_list = self.stat_dataset(tmp_normal_data,
                                                                                                   dtype=0)
            aug_normal_path_list, aug_normal_label_list, aug_normal_id_list = real_normal_path_list, [2] * len(
                real_normal_label_list), real_normal_id_list
            anomaly_path_list, anomaly_label_list, anomaly_id_list = real_normal_path_list, [1] * len(
                real_normal_label_list), real_normal_id_list

            normal_path_list = real_normal_path_list + aug_normal_path_list
            normal_label_list = real_normal_label_list + aug_normal_label_list
            normal_id_list = real_normal_id_list + aug_normal_id_list
        else:
            if self.opt.test_result:
                self.anomaly_json_path = os.path.join(self.root_dir, 'test.json')
                self.normal_json_path = os.path.join(self.root_dir, 'test_normal.json')
            else:
                self.anomaly_json_path = os.path.join(self.root_dir, 'valid.json')
                self.normal_json_path = os.path.join(self.root_dir, 'valid_normal.json')

            tmp_anomaly_data = json.load(open(self.anomaly_json_path))
            if self.opt.target_focus > -1:
                tmp_anomaly_data = [item for item in tmp_anomaly_data if self.opt.target_focus in item['label']]
            random.shuffle(tmp_anomaly_data)
            if self.opt.target_num > 0:
                tmp_anomaly_data = tmp_anomaly_data[:self.opt.target_num]

            print('abnormal scan num:', len(tmp_anomaly_data))
            anomaly_path_list, anomaly_label_list, anomaly_id_list = self.stat_dataset(tmp_anomaly_data, dtype=1)

            if self.opt.only_target_slice:
                normal_path_list, normal_label_list, normal_id_list = [], [], []
                print('only test/valid with target slice, normal scan num: 0')
            else:
                tmp_normal_data = json.load(open(self.normal_json_path))
                random.shuffle(tmp_normal_data)
                if self.opt.neg_ratio > 0:
                    tmp_normal_data = tmp_normal_data[:int(self.opt.neg_ratio * len(tmp_anomaly_data))]
                print('normal scan num:', len(tmp_normal_data))

                normal_path_list, normal_label_list, normal_id_list = self.stat_dataset(tmp_normal_data, dtype=0)

        self.path_list = anomaly_path_list + normal_path_list
        self.label_list = anomaly_label_list + normal_label_list
        self.id_list = anomaly_id_list + normal_id_list

        print('total slice: ', len(self.path_list))
        print('total 1 label slice: ', sum(self.label_list))

        p = list(zip(self.path_list, self.label_list, self.id_list))
        random.shuffle(p)
        self.path_list, self.label_list, self.id_list = [list(item) for item in zip(*p)]

        print('creating samples')
        transform_list = []
        transform_list += [transforms.ToTensor()]
        transform_list += [transforms.Normalize((0.5,), (0.5,))]
        self.trans = transforms.Compose(transform_list)

    def __getitem__(self, idx):
        data = self.load_sample(idx)
        while data is None:
            # print('the item is wrong: ', self.path_list[idx])
            idx = (idx + 1) % len(self.path_list)
            data = self.load_sample(idx)
        return data

    def __len__(self):
        return len(self.path_list)

    def load_sample(self, idx):
        p = self.path_list[idx]
        label = self.label_list[idx]
        try:
            img = Image.open(p).convert("RGB")
            img = np.array(img)
            img = cv2.resize(img, (256, 256))
        except:
            return None

        gray = img[..., 0]  # HW

        scan_id = os.path.basename(p).split('-')[1]
        slice_id = os.path.splitext(os.path.basename(p))[0].split('-')[-1]

        if self.data_type == 'train':
            if label == 1:
                gray = self.gen_aug_pos(gray)
            elif label == 2:
                gray = self.gen_aug_neg(gray)
        img = np.stack([gray] * 3, axis=-1)

        '''step 1: create edge'''
        if self.opt.multi_edge:
            info_mask_list = []
            for (canny_thred_1, canny_thred_2) in self.canny_list[:self.opt.edge_mask_num]:
                tmp_edge = cv2.Canny(img[..., 0], canny_thred_1, canny_thred_2, apertureSize=3)  # 60是最小阈值,150是最大阈值
                info_mask_list.append(tmp_edge)

            if len(info_mask_list) == 0:
                edge = None
            else:
                edge = np.stack(info_mask_list, axis=0)
        else:
            canny_thred_1, canny_thred_2 = random.choice(self.canny_list[:self.opt.edge_mask_num])
            edge = cv2.Canny(gray, canny_thred_1, canny_thred_2, apertureSize=3)  # 60是最小阈值,150是最大阈值

        if self.opt.filter_black:
            black_pixels = (gray == 0).astype('int').sum()
            total_pixels = gray.shape[0] * gray.shape[1]
            if black_pixels >= total_pixels * self.opt.black_thred:
                return None

        '''step 2: norm img'''
        img = self.to_tensor(img)
        gray = self.to_tensor(gray)

        '''step 3: select a mask'''
        imgs = torch.stack([img] * (self.mask_num ** 2), dim=0)
        grays = torch.stack([gray] * (self.mask_num ** 2), dim=0)
        if edge is not None:
            edge = torch.Tensor(edge) / 255.
            edges = torch.stack([edge] * (self.mask_num ** 2), dim=0)
        else:
            edges = torch.zeros_like(imgs)
        return imgs, grays, edges, self.masks, self.masks, label, p, scan_id, slice_id

    def next_epoch(self):
        self.__init__(self.opt, self.data_type)

    def to_tensor(self, img):
        img = Image.fromarray(img)
        img_t = F.to_tensor(img).float()
        return img_t

    def gen_aug_pos(self, gray):
        cnt = 10
        orgimg = copy.deepcopy(gray)
        while cnt > 0:
            cnt -= 1
            if random.random() < 0.5:
                center_x = random.randint(10, gray.shape[1] - 10)
                center_y = random.randint(30, gray.shape[0] - 30)
                pp = random.randint(4, 50)
                plocs = []
                for ps in range(pp):
                    plocs.append([center_x + random.randint(-10, 10), center_y + random.randint(-10, 10)])
                plocs = np.array(plocs)

                mask = np.zeros(gray.shape)
                nbasis = np.random.random(gray.shape) * 0.2
                if random.random() <= 0.8:
                    noise = 255 * nbasis
                else:
                    noise = 255 * (1 - nbasis)

                cv2.polylines(mask, [plocs], 1, 255)  # 描绘边缘
                cv2.fillPoly(mask, [plocs], 255)  # 填充
                cv2.GaussianBlur(noise.astype('uint8'), (3, 3), 0)
                orgimg = np.where(mask == 0, orgimg, noise)

            elif random.random() < 0.9:
                block_noise_size_x = random.randint(3, 20)
                block_noise_size_y = random.randint(3, 20)
                noise_x = random.randint(3, gray.shape[1] - block_noise_size_x - 3)
                noise_y = random.randint(30, gray.shape[0] - block_noise_size_y - 30)
                if random.random() <= 0.8:
                    nbasis = np.random.random((block_noise_size_y, block_noise_size_x)) * 0.2
                else:
                    nbasis = 1 - np.random.random((block_noise_size_y, block_noise_size_x)) * 0.2

                orgimg[noise_y:noise_y + block_noise_size_y, noise_x:noise_x + block_noise_size_x] = nbasis
                cnt -= 1

            else:
                cnt -= 1

        return orgimg.astype('uint8')

    def gen_aug_neg(self, gray):
        orgimg = copy.deepcopy(gray)
        img_row, img_col = gray.shape
        if random.random() < 0.8:
            center = (img_col // 2, img_row // 2)  # center rotate
            angle = random.randint(-45, 45)  # rotate angle
            scale = random.uniform(0.75, 1.0)  # scale
            M = cv2.getRotationMatrix2D(center, angle, scale)
            orgimg = cv2.warpAffine(src=orgimg, M=M, dsize=gray.shape)
        if random.random() < 0.8:
            orgimg = cv2.flip(orgimg, 1)  # horizontal flip

        return orgimg.astype('uint8')

    def stat_dataset(self, dataset, dtype=0):
        path_list = []
        label_list = []
        id_list = []
        for i, sample in enumerate(dataset):
            slice_paths = sample['dcm_paths']
            slice_ids = [k for k in range(len(slice_paths))]
            tmp_labels = [dtype] * len(slice_paths)
            path_list.extend(slice_paths)
            label_list.extend(tmp_labels)
            id_list.extend(slice_ids)

        return path_list, label_list, id_list

    def prepare_masks(self):
        mask_list = []
        width = self.opt.load_size // self.mask_num
        for i in range(self.mask_num):
            for j in range(self.mask_num):
                mask = np.zeros((self.opt.load_size, self.opt.load_size))
                mask[width * i : width * (i + 1), width * j : width * (j + 1)] = 255
                mask = mask.astype('uint8')
                mask = self.to_tensor(mask)
                mask_list.append(mask)
        masks = torch.stack(mask_list)
        return masks