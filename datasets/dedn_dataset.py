import cv2
from util.basic import *
from util.video import *
from .base_dataset import BaseDataset
import torchvision.transforms as transforms
import torchvision.transforms.functional as F
import numpy as np
class DeDNDataset(BaseDataset):
    @staticmethod
    def modify_commandline_options(parser):

        parser.add_argument('--target_focus', type=int, default=0)
        parser.add_argument('--target_focus_name', type=str, default='')
        parser.add_argument('--test_with_train_normal', type=int, default=0)
        parser.add_argument('--window_type', type=str, default='mid2high')
        parser.add_argument('--drop_slice_num', type=int, default=0)
        parser.add_argument('--resize_dcm_len', type=int, default=30)
        parser.add_argument('--cut_hu', type=int, default=200)
        parser.add_argument('--multi_ww', type=int, default=0)
        parser.add_argument('--norm_data', type=int, default=0)
        parser.add_argument('--sigma', type=float, default=1)
        parser.add_argument('--mask_num', type=int, default=4)
        parser.add_argument('--load_size', type=int, default=256)
        parser.add_argument('--only_target_slice', type=int, default=0)
        parser.add_argument('--with_lunkuo', type=int, default=0)
        parser.add_argument('--with_naoshi_info', type=int, default=1)
        parser.add_argument('--lunkuo_num', type=int, default=0)
        parser.add_argument('--max_mask_num', type=int, default=50)

        parser.add_argument('--target_num', type=int, default=0)
        parser.add_argument('--canny_type', type=int, default=0)
        parser.add_argument('--ww_type', type=str, default='blood')
        parser.add_argument('--canny_thred_1', type=int, default=60)
        parser.add_argument('--canny_thred_2', type=int, default=150)

        parser.add_argument('--input', type=str, help='path to the input images directory or an input image')
        parser.add_argument('--mask', type=str, help='path to the masks directory or a mask file')
        parser.add_argument('--edge', type=str, help='path to the edges directory or an edge file')
        parser.add_argument('--output', type=str, help='path to the output directory')
        return parser

    def __init__(self, opt, data_type):
        BaseDataset.__init__(self, opt, data_type)

        self.data_type = data_type

        self.data_norm_type = 'gray'
        self.image_names = ['input', 'previous', 'later']

        '''type 1 canny that cound be useful'''

        self.img_resize = BaseDataset.get_aug_transform(opt, preprocess='resize', in_type='HWC', out_type='HWC')
        self.mask_num = self.opt.mask_num
        self.masks = self.prepare_masks()
        self.canny_list = [(20, 100), (80, 140), (220, 240)]

        self.root_dir = 'data/oct_paths'
        
        if data_type == 'train':
            json_path = os.path.join(self.root_dir, 'train_normal.json')
            with open(json_path) as f:
                self.dataset_json = json.load(f)
        else:
            random.seed(9)
            random.shuffle(self.dataset_json)
            anomaly_json_path = os.path.join(self.root_dir, 'test.json')
            normal_json_path = os.path.join(self.root_dir, 'test_normal.json')
            tmp_anomaly_data = json.load(open(anomaly_json_path))
            tmp_anomaly_data = list(filter(lambda x: self.opt.target_focus in x['label'], tmp_anomaly_data))

            tmp_normal_data = json.load(open(normal_json_path))[:len(tmp_anomaly_data)]
            self.dataset_json = tmp_anomaly_data + tmp_normal_data

        random.shuffle(self.dataset_json)
        self.path_list, self.label_list = self.stat_dataset(self.dataset_json)

        print('file num', len(self.path_list))
        print('creating samples')
        transform_list = []
        transform_list += [transforms.ToTensor()]
        transform_list += [transforms.Normalize((0.5,), (0.5,))]
        self.trans = transforms.Compose(transform_list)

    def gen_masks(self):
        min_num = int(self.opt.mask_num * 0.1)
        max_num = int(self.opt.mask_num * 0.5)
        mask_num = random.randint(min_num, max_num)
        mask_inds = random.sample(list(range(self.mask_num ** 2)), k= mask_num)
        mask = np.zeros((self.opt.load_size, self.opt.load_size))

        width = self.opt.load_size // self.mask_num
        for mask_ind in mask_inds:
            i = mask_ind // self.mask_num
            j = mask_ind % self.mask_num
            mask[width * i : width * (i + 1), width * j : width * (j + 1)] = 255
        mask = mask.astype('uint8')
        mask = self.to_tensor(mask)
        return mask


    def __getitem__(self, idx):
        data = self.load_sample(idx)
        while data is None:
            print('the item is wrong')
            idx = (idx + 1) % len(self.path_list)
            data = self.load_sample(idx)
        return data

    def __len__(self):
        return len(self.path_list)

    def load_sample(self, idx):
        p = self.path_list[idx]
        label = self.label_list[idx]
        try:
            img = Image.open(p).convert("RGB")
            img = np.array(img)
            img = cv2.resize(img, (256, 256))
        except:
            return None

        gray = img[..., 0]

        scan_id = os.path.basename(p).split('-')[1]


        '''step 1: create edge'''

        if self.opt.multi_edge:
            info_mask_list = []
            for (canny_thred_1, canny_thred_2) in self.canny_list[:self.opt.edge_mask_num]:
                tmp_edge = cv2.Canny(img[...,0], canny_thred_1, canny_thred_2, apertureSize=3)
                info_mask_list.append(tmp_edge)

            if len(info_mask_list) == 0:
                edge = None
            else:
                edge = np.stack(info_mask_list, axis=0)
        else:
            canny_thred_1, canny_thred_2 = random.choice(self.canny_list[:self.opt.edge_mask_num])
            edge = cv2.Canny(gray, canny_thred_1, canny_thred_2, apertureSize=3)

        '''step 2: norm img'''
        img = self.to_tensor(img)
        gray = self.to_tensor(gray)

        if edge is not None:
            edge = torch.Tensor(edge) / 255.

        '''step 3: select a mask'''
        if self.data_type == 'train':
            mask = self.gen_masks()

            if edge is None:
                edge = 1

            return img, gray, edge, mask, mask, label, p, scan_id
        else:
            imgs = torch.stack([img] * (self.mask_num ** 2), dim = 0)
            grays = torch.stack([gray] * (self.mask_num ** 2), dim = 0)

            if edge is not None:
                edges = torch.stack([edge] * (self.mask_num ** 2), dim = 0)
            else:
                edges = 1

            return imgs, grays, edges, self.masks, self.masks, label, p, scan_id

    def next_epoch(self):
        self.__init__(self.opt, self.data_type)

    def to_tensor(self, img):
        img = Image.fromarray(img)
        img_t = F.to_tensor(img).float()
        return img_t

    def stat_dataset(self, json_path):
        path_list = []
        label_list = []
        scan_label_list = [0,0]
        for i, sample in enumerate(json_path):
            slice_paths = sample['dcm_paths']

            if self.opt.target_focus in sample['label']:
                label = 1
            else:
                label = 0
                
            tmp_slice_labels = [label] * len(slice_paths)
            scan_label_list[label] += 1

            path_list.extend(slice_paths)
            label_list.extend(tmp_slice_labels)

        return path_list, label_list

    def prepare_masks(self):
        mask_list = []
        width = self.opt.load_size // self.mask_num
        for i in range(self.mask_num):
            for j in range(self.mask_num):
                mask = np.zeros((self.opt.load_size, self.opt.load_size))
                mask[width * i : width * (i + 1), width * j : width * (j + 1)] = 255
                mask = mask.astype('uint8')
                mask = self.to_tensor(mask)
                mask_list.append(mask)
        masks = torch.stack(mask_list)
        return masks