# Disorder-Free Data are All You Need: Inverse Supervised Learning for Broad-Spectrum Head Disorder Detection

## Introduction

This project contains source code and data for our manuscript "Disorder-Free Data are All You Need: Inverse Supervised Learning for Broad-Spectrum Head Disorder Detection".

<img src="fig/ISL.png" style="zoom:25%;" />

## Requirements 

The important packages are recorded in `requirements.txt`

## Get Started

Here we take developing a disorder detection system for retinal OCT images as a example.


### step 1: Prepare the data

OCT data can be downloaded from https://data.mendeley.com/datasets/rscbjbr9sj/2. The downloaded data should be extracted in `./data/OCT2017`

### step 2: Training a DeDN model

This step aims at developing a DeDN model that can generate de-disorder images:

```
python main_bash.py -s train_dedn
```

The developed model will be stored in `checkpoints/{dedn_setting_info}`, where the value `dedn_setting_info` is related to the information in `setting/train_dedn.py`.

### step 3: Training a DRN model

This step aims at developing a DRN model that can evaluate the disorder-containing probabilites.

This step required the developed DeDN model:

```
python main_bash.py -s train_drn --aux_params dedn_load_dir&checkpoints/{dedn_setting_info}
```
